<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#info" aria-controls="info" role="tab" data-toggle="tab">Information</a></li>
</ul>
<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="info">
        <table id = "users" class="table table-hover table-bordered ui-widget ui-widget-content">
            <thead>
                <tr class = "first ui-widget-header">
                    <td><?= $this->Paginator->sort('Redmine_ID');?></td>
                    <td><?= $this->Paginator->sort('release_date');?></td>
                    <td><?= $this->Paginator->sort('user_release');?></td>
                    <td><?= $this->Paginator->sort('title_release');?></td>
                    <td><span class="glyphicon glyphicon-hand-down" aria-hidden="true"></span></td>
                </tr>
            </thead>
            <tbody>
            <?php 

            foreach($ele_release as $key =>$value){
            //debug($ele_release);
            
            ?> <!-- show data !-->
                <tr>
                    <td id="redmine_id<?= $value['id']; ?>"><?= $value['redmine_id']; ?></td>
                    <td id="release_date<?= $value['id']; ?>"><?= $value['release_date']; ?></td>
                    <td><?=$value['m']['full_name']; ?></td>
                    <td style= "word-break: break-all;"><?= mb_substr($value['title_release'],0,10); ?>
                        <input type = "hidden" id="title_release<?= $value['id']; ?>" value="<?= $value['title_release']; ?>" />
                    </td>
                    <td>
                    <?= $this->Html->link((''),['action'=>'details',$value['id']],array('class' => 'glyphicon glyphicon-list-alt')); ?>
                    <div style = " margin-left: 15px; margin-right:15px; ; cursor:pointer;" class="glyphicon glyphicon-trash" onclick="deleteRelease(<?= $value['id']; ?>);">    
                    </div>
                    <div style = "cursor:pointer;" onclick="editRelease(<?= $value['id']; ?>)" class="glyphicon glyphicon-wrench"> 
                    </div>
                       <input type = "hidden" id="user_release<?= $value['id']; ?>" value="<?= $value['user_release']; ?>" />
                        <div style = "display:none;">
                         <input type = "hidden" id="inlineCheckbox1<?= $value['id']; ?>" value="<?= $value['has_change_db']; ?>" />
                         <input type = "hidden" id="inlineCheckbox2<?= $value['id']; ?>" value="<?= $value['db_backup']; ?>" />
                    <input type = "hidden" id="status<?= $value['id']; ?>" value="<?= $value['status']; ?>" /></div>
                    </td>
                  
                </tr>
            <?php } ?>
            </tbody>
        </table>
         <div class="paginator" >
            <ul class="pagination">
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
            </ul>
            <p><?= $this->Paginator->counter() ?></p>
        </div>
        <button id="create-user" class = "btn btn-success">Create New Release</button>
        <?= $this->element('addRelease'); ?>
    </div>
</div>
<div style=" display: none;" id="dialog-confirm-deleteRelease" class="dialog-confirm-deleteRelease" title="Are you delete?">
<p><span class="ui-icon ui-icon-trash" style=" float:left; margin:0 7px 20px 0;"></span>Your Release Note will be deleted. Are you sure?</p>
</div>
