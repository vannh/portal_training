    <?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;
?>
<input id="release_id" style = "display: none;" value="<?= $ele_release->id ?>"> 
<div align="left" class = "alert alert-success" >Information</div>
<div style = "border:inset 1px black;"></div>
<table style = "border:outset 1px black;" class="table table-condensed table-bordered">
    <tr class="danger">
        <th>Redmine_Id</th>
        <td>
            <h1> <?= $ele_release['redmine_id']; ?></h1>
        </td>
    </tr>
    <tr class="active">
        <th>Release_Date</th>
            <td><?= $ele_release['release_date']; ?></td>
    </tr>
     <tr class="info">
        <th>Title_Release</th>
            <td><?= $ele_release['title_release']; ?></td>
    </tr>
    <tr>
        <th>User_Release</th>
            <td><?= $ele_release['user_release']; ?></td>
    </tr>
    <tr >
        <th>Has_Change_Db</th>
            <td><?= $db[(int)$ele_release['has_change_db']]; ?></td>
    </tr>
    <tr>
        <th>Db_Backup</th>
            <td><?= $db[(int)$ele_release['db_backup']]; ?></td>
    </tr>
    <tr>
        <th>Status</th>
            <td>
            <?php $st = isset($status[$ele_release['status']]) ? $status[$ele_release['status']] : ""; ?> 
            <?= $st ?>
            </td>
    </tr>
    <tr>
        <th>Created</th>
            <td><?= date_format($ele_release['created'],'d/m/Y H:i:s'); ?></td>
    </tr>
    <tr>
        <th>Modified</th>
            <td><?= date_format($ele_release['modified'],'d/m/Y H:i:s'); ?></td>
    </tr>
</table>
<hr>
<br>
<br>
<div align = "left" class = "alert alert-danger">List Task </div>
<div align = "right"><div class = "glyphicon glyphicon-tasks" id="addTask"></div>
<table id = "example1" class="table table-hover tmp_table2">
    <thead> 
        <tr>
            <th style = "background: #EA4335;text-align: center; border:inset 1px black ">Redmine_id</th>
            <th style = "background: #EA4335;text-align: center; border:inset 1px black">Assigned</th>
            <th style = "background: #EA4335;text-align: center; border:inset 1px black">Title</th>
            <th style = "background: #EA4335;text-align: center; border:inset 1px black">Action</th>
        </tr>
    </thead>
    <tbody class = "tmp2">
    <?php foreach($tmp as $key => $data) { 
    ?>
        <tr>
            <td><?= $data['redmine_id'] ; ?></td>
            <td><?= $data['m']['full_name']; ?></td>
            <td><?= $data['title']; ?></td>
            <td>
                <div style = "cursor:pointer;" class="glyphicon glyphicon-trash" onclick="deleteReleaseTask(<?= $data['r']['id']; ?>);">
                </div>
            </td>
        </tr>
    <?php 
    }
    ?>
    </tbody>
</table>
<hr>
<br>
<br>
<div align = "left"> <div class = "alert alert-info" > List File </div></div>
<div align = "right"><div class = "glyphicon glyphicon-plus" id="addFile"></div>
<table id = "example2" class="table table-hover tmp_table2">
    <thead> 
        <tr>
            <th style = "background: #FBBC05;text-align: center; border:inset 1px black; width:10%; ">STT</th>
            <th style = "background: #FBBC05;text-align: center; border:inset 1px black; width:40%;">File_Path</th>
            <th style = "background: #FBBC05;text-align: center; border:inset 1px black; width:20%; ">State</th>
            <th style = "background: #FBBC05;text-align: center; border:inset 1px black; width:20%;">Coder</th>
            <th style = "background: #FBBC05;text-align: center; border:inset 1px black; width:10%;">Action</th>
        </tr>
    </thead>
    <tbody class = "tmp2">
    <?php $i = 1; 
     foreach($data_file as $key => $data){
         ?>
        <tr>
            <td id="stt"  style = "width:10%;"><?= $i++; ?></td>
            <td id="File_Path<?= $data['f']['id']; ?>" style = "width:40% ;word-break:break-all;"><?= $data['f']['file_path']; ?></td>
            <td  style = "width:20%;" ><?= $state[$data['f']['state']]; ?></td>
            <td style = "width:20%;"><?= $data['m']['full_name']; ?></td>
            <td style = "width:10%;">
                <div style ="cursor:pointer;" class="glyphicon glyphicon-trash" onclick="DeleteReleaseFile(<?= $data['f']['id']; ?>);"></div>
                <div style = "cursor:pointer; margin-left: 10px;" onclick="EditReleaseFile(<?= $data['f']['id']; ?>)" class="glyphicon glyphicon-wrench"></div>
                 <div style = "display:none;">
                    <input type = "hidden" id="state<?= $data['f']['id']; ?>" value="<?= $data['f']['state']; ?>" />
                    <input type = "hidden" id="coder<?= $data['f']['id'];  ?>" value="<?= $data['f']['coder']; ?>" />
                 </div>
            </td>
         </tr>
        <?php
        } 
        ?>
    </tbody>
</table>
<hr>
<br>
<br>
<div align = "left"> <div class = "alert alert-info" style = "background:#34A853"> Change DataBase </div></div>
<div align = "right"><div class = "glyphicon glyphicon-plus" id="add_databases"></div>
<table id = "example3" class="table table-hover tmp_table2">
    <thead>
        <tr>
            <th style = "background: #34A853;text-align: center; border:inset 1px black; width:20%; ">Editor</th>
            <th style = "background: #34A853;text-align: center; border:inset 1px black; width:40%;">Query</th>
            <th style = "background: #34A853;text-align: center; border:inset 1px black; width:20%; ">Table</th>
            <th style = "background: #34A853;text-align: center; border:inset 1px black; width:15%;">Status</th>
            <th style = "background: #34A853;text-align: center; border:inset 1px black; width:15%;">Action</th>
        </tr>
    </thead>
    <tbody class = "tmp2">
    <?php 
    foreach($database_data as $key =>$data) 
    
    {
    ?>
        <tr>
            <td style = "width:20%; vertical-align:middle;"><?= $Type_Database[$data['d']['editor']]; ?></td>
            <td style = "width:40% ; vertical-align:middle; word-break:break-all;"><?= mb_substr($data['d']['query_db'],0,20); ?></td>
            <td id="table_db<?= $data['d']['id']; ?>"  style = "width:20%;vertical-align:middle;"><?= $data['d']['table_db']; ?></td>
            <td  style = "width:15%;vertical-align:middle;"><?= $Status_DB[$data['d']['status']]; ?></td>
            <td style = "width:15%;">
                 <div style = "display:none;">
                    <input type = "hidden" id="editor_db<?= $data['d']['id']; ?>" value="<?= $data['d']['editor']; ?>" />
                    <input type = "hidden" id="status_db<?= $data['d']['id'];  ?>" value="<?= $data['d']['status']; ?>" />
                 </div>
                <div style ="cursor:pointer; vertical-align:middle;" class="glyphicon glyphicon-trash"  onclick="DeleteReleaseDB(<?= $data['d']['id']; ?>);"></div>
                <div style = "cursor:pointer;vertical-align:middle; margin-left: 10px;" onclick="EditReleaseDB(<?= $data['d']['id']; ?>)" class="glyphicon glyphicon-wrench"></div>
                <div style = "cursor:pointer;vertical-align:middle; margin-left: 10px;" onclick="show_query(<?= $data['d']['id']; ?>)" class="glyphicon glyphicon-list-alt"></div>
                <div style=" display: none;word-break:break-all;" id="dialog-show-query<?= $data['d']['id'];  ?>" class="dialog-confirm" title="Show Query"><?= $data['d']['query_db']; ?></div>
            </td>
         </tr>
    <?php
    } 
    ?>
    </tbody>
</table>
<hr>
<br>
<br>
<div align = "left"> <div class = "alert alert-info" style = "background:#4285F4;"> Expected </div></div>
<div align = "right"><div class = "glyphicon glyphicon-plus" id="add_expected"></div>
<table id = "example4" class="table table-hover tmp_table2">
    <thead>
        <tr>
            <th style = "background: #4285F4;text-align: center; border:inset 1px black; width:20%; ">Time</th>
            <th style = "background: #4285F4;text-align: center; border:inset 1px black; width:40%;">Description</th>
            <th style = "background: #4285F4;text-align: center; border:inset 1px black; width:20%; ">Assignee</th>
            <th style = "background: #4285F4;text-align: center; border:inset 1px black; width:15%;">Review</th>
            <th style = "background: #4285F4;text-align: center; border:inset 1px black; width:15%;">Action</th>
        </tr>
    </thead>
    <tbody class = "tmp2">
    <?php foreach($query_expected as $key => $data)
    {
    ?>
        <tr>
            <td id = "time<?= $data['e']['id']; ?>" style = "width:20%; vertical-align:middle;"><?= $data['e']['time_expected']; ?></td>
            <td id = "action<?= $data['e']['id']; ?>" style = "width:40% ; vertical-align:middle; word-break:break-all;"><?= $data['e']['action']; ?></td>
            <td style = "width:20%;vertical-align:middle;"><?= $data['m']['full_name']; ?></td>
            <td style = "width:15%;vertical-align:middle;"><?= $data['tmp']['full_name']; ?></td>
            <td style = "width:15%;">
                 <div style = "display:none;">
                    <input type = "hidden" id = "status_process<?= $data['e']['id']; ?>" value="<?= $data['e']['status']; ?>" />
                    <input type = "hidden" id = "assignee<?= $data['e']['id']; ?>" value="<?= $data['e']['assignee']; ?>" />
                    <input type = "hidden" id="review<?= $data['e']['id'];  ?>" value="<?= $data['e']['review']; ?>" />
                 </div>
                  <div style=" display: none;word-break:break-all;" id = "note<?=$data['e']['id']; ?>"><?= $data['e']['note']; ?></div>
                <div style ="cursor:pointer; vertical-align:middle;" class="glyphicon glyphicon-trash"  onclick="DeleteReleaseExpected(<?= $data['e']['id']; ?>);"></div>
                <div style = "cursor:pointer;vertical-align:middle; margin-left: 10px;" onclick="EditReleaseExpected(<?= $data['e']['id']; ?>)" class="glyphicon glyphicon-wrench"></div>
                <div style = "cursor:pointer;vertical-align:middle; margin-left: 10px;"  onclick="show_expected(<?= $data['e']['id']; ?>)" class="glyphicon glyphicon-list-alt"></div>
               <div style=" display: none;word-break:break-all;" id="dialog-show-expected<?= $data['e']['id']; ?>" class="dialog-confirm" title="Show Expected">
                    <label style = "display: inline;">Time</label>
                        <p style ="float:right;"> <?= $data['e']['time_expected']; ?></p>
                    <div class = "clearfix"></div>
                    <hr>
                    <label>Action</label>
                        <p> <?= $data['e']['action']; ?></p>
                    <hr>       
                    <label style = "display: inline;">Assignee</label>
                        <p style ="float:right;"> <?= $data['m']['full_name']; ?></p>
                    <div class = "clearfix"></div>
                    <hr>
                    <label style = "display: inline;">Review</label>
                        <p style ="float:right;"><?= $data['tmp']['full_name']; ?></p>
                    <div class = "clearfix"></div> 
                    <hr>
                    <label style = "display: inline;">Status</label>
                        <p style ="float:right;"> <?= $status_process[$data['e']['status']]; ?></p>
                    <div class = "clearfix"></div>
                    <hr>
                     <label>Comment</label>
                    <textarea style=" border: none; background:#fff"; class="form-control resize" rows = '7' readonly ><?= $data['e']['note']; ?></textarea>
               </div>
            </td>
         </tr>
    <?php
    } 
    ?>
    </tbody>
</table>
<p align="center" > <?= $this->Html->link(('Return'),['action'=>'index'],['class' => 'btn btn-primary']); ?></p>
<?= $this->element('addtask'); ?>
<?= $this->element('addfile'); ?>
<?= $this->element('add_database_release'); ?>
<?= $this->element('ReleaseExpected'); ?>
<div style=" display: none;" id="dialog-confirm-Releasetask" class="dialog-confirm" title="Are you delete?">
<p><span class="ui-icon  ui-icon-trash" style=" float:left; margin:0 7px 20px 0;"></span>Your Task will be deleted. Are you sure?</p>
</div>
<div style=" display: none;" id="dialog-confirm-file" class="dialog-confirm" title="Are you delete?">
<p><span class="ui-icon  ui-icon-trash" style=" float:left; margin:0 7px 20px 0;"></span>Your File will be deleted. Are you sure?</p>
</div>
<div style=" display: none;" id="dialog-confirm-db" class="dialog-confirm" title="Are you delete?">
<p><span class="ui-icon  ui-icon-trash" style=" float:left; margin:0 7px 20px 0;"></span>Your Option will be deleted. Are you sure?</p>
</div>
<div style=" display: none;" id="dialog-confirm-expected" class="dialog-confirm" title="Are you delete?">
<p><span class="ui-icon  ui-icon-trash" style=" float:left; margin:0 7px 20px 0;"></span>Your Expected will be deleted. Are you sure?</p>
<script>