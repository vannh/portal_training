<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;

if (!Configure::read('debug')):
    throw new NotFoundException();
endif;

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<body>
<div id="wrapper">
	<div class="notification-ringi">
		<div class="clear"></div>
	</div>
    <fieldset class="fsStyle">
        <legend class="legendStyle">
            <?= __('Tasks of team') ?>
            <div class="msg1" style="display: inline; font-size: 15px;"></div>
			<span style="float: right;" >
				<a data-toggle="modal" data-target="#memberModal" data-title="add">
					<span class="glyphicon glyphicon-plus"></span>
				</a>
			</span>
        </legend>
	
	<?php if(count($task) > 0): ?>
	<div id = "list">
	<table class = "table table-striped table-bordered table-hover">
			<thead>
		     	<tr class="danger" >
		     		<th style="width: 100px">Số thứ tự</th>
			        <th style="width: 183px">Redmine_id</th>
			        <th style="width: 183px">Assigned</th>
			        <th style="width: 183px">Title</th>
			        <th style="width: 183px">Ngày tạo</th>
			        <th style="width: 183px">Ngày chỉnh sửa</th>
			        <th style="width: 183px">Status</th>
			        <th style="width: 183px">Active</th>
		     	</tr>
		    </thead>

        	<tbody>
        	<?php
        	$dem = 0;
 				foreach ($task as $key => $dataTask) { 
 					$dem++;
 					$created = date("Y-m-d h:i:s", strtotime($dataTask->created));
 					if (empty($dataTask->modified)) {
 						$modified = '0000-00-00 00:00:00';
 					}else{
 						$modified = date("Y-m-d H:i:s", strtotime($dataTask->modified)); 
 					}?>
							<tr class = "info">	
								<td style = "text-align: center;"><?= $dem ?></td>
								
								<td><?= $this->Html->link(__($dataTask->redmine_id), ['action' => 'chitiet', $dataTask->id]) ?></td>
								<td id='<?= $dataTask->id ?>'> <?=  $arrMembers[$dataTask->assigned] ?></td>
								<td id="title_task<?= $dataTask->id ?>"><?= $dataTask->title ?></td>
								
								<td id='<?= $dataTask->id ?>'> <?= $created ?> </td>
								<td id='<?= $dataTask->id ?>'> <?= $modified ?> </td>
								<td id='<?= $dataTask->id ?>'><?=  $task_status[$dataTask->status]?> </td>
								<td >
									<a class="btn btn-success" data-toggle="modal" data-target="#memberModal" data-title="edit" data-member='<?= $dataTask->id ?>'>
											<span class="glyphicon glyphicon-pencil"></span>
									</a>
									<a class="btn btn-primary" onclick="deleteTask(<?= $dataTask->id ?>);">
											<span class="glyphicon glyphicon-trash"></span>
									</a>
								</td>
								<div style = "display:none;">
								<input value = "<?= $dataTask->assigned ?>"  id='assigned<?= $dataTask->id ?>'> </input>
								<input value = "<?= $dataTask->status ?>"  id='task_status<?= $dataTask->id ?>'> </input>
								<p id='redmine_id<?= $dataTask->id ?>'> <?= $dataTask->redmine_id ?> </p>
								<input value = "<?= $dataTask->doc_file ?>"  id='doc_file<?= $dataTask->id ?>'> </input>
								<input value = "<?= $dataTask->member_review ?>"  id='member_review<?= $dataTask->id ?>'> </input>
								<input value = "<?= $dataTask->project_id ?>"  id='project_id<?= $dataTask->id ?>'> </input>
								</div>
								<td style = "display:none;" id='task_goal<?= $dataTask->id ?>'> <?= $dataTask->task_goal ?></td>
								<td style = "display:none;" id='doc_file<?= $dataTask->id ?>'> <?= $dataTask->doc_file ?></td>
								<td style = "display:none;" id="test_cases<?= $dataTask->id ?>"><?= $dataTask->test_cases ?></td>
								<td style = "display:none;" id='review_testcase<?= $dataTask->id ?>'> <?= $dataTask->review_testcase ?></td>
								<td style = "display:none;" id='merge_req_test<?= $dataTask->id ?>'> <?= $dataTask->merge_req_test ?></td>
								<td style = "display:none;" id='merge_reg_staging<?= $dataTask->id ?>'> <?= $dataTask->merge_reg_staging ?></td>
								<td style = "display:none;" id="merge_reg_live<?= $dataTask->id ?>"><?= $dataTask->merge_reg_live ?></td>
							</tr>
				<?php } ?>
        	</tbody>
    </table>
    </div>

    <?php else: ?>
            <?= __('Have no task, please add') ?>
        <?php endif ?>
    </fieldset>
</div>
<?= $this->element('add_task'); ?>
