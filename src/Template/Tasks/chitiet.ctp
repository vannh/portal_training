<div class="container">
  <div class="row">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3><strong>Chi tiết task </strong>
          <?php if ($task->status == 2): ?>
          <strong style="float: right;"><?= $task_status[$task->status]?></strong> <?php else: ?>
          <strong style="float: right;"><?= $task_status[$task->status]?></strong> <?php endif ?>
          </h3>

        </div>  
        <table class="table table-fixed table-hover">
          
          <tbody>
             <tr>
              <td class="col-sm-2"> <strong>Project</strong></td>
              <td class="col-sm-10"> <?= $task->project_id ?></td>
            </tr>
            <tr>
              <td><strong>Redmine_ID</strong></td>
              <td><?= $task->redmine_id ?></td>
            </tr>
           
            <tr>
              <td><strong>Title</strong></td>
              <td><?= $task->title ?></td>
            </tr>
            <tr>
              <td><strong>Mục đích task</strong></td>
              <td><?= $task->task_goal ?></td>
            </tr>
            <tr>
              <td><strong>Người thực hiện</strong></td>
              <td><?= $arrMembers1[$task->assigned] ?></td>
            </tr>
            <tr>
              <td><strong>Người review</strong></td>
              <td><?= $arrMembers1[$task->member_review] ?></td>
            </tr>
            
            <tr>
              <td><strong>Test cases</strong></td>
              <td><?= $task->test_cases ?></td>
            </tr>
            <tr>
              <td><strong>Review test case</strong></td>
              <td><?= $task->review_testcase ?></td>
            </tr>
            <tr>
              <td><strong>Merge_req_test</strong></td>
              <td><?= $task->merge_req_test ?></td>
            </tr>
            <tr>
              <td><strong>Merge_reg_staging</strong></td>
              <td><?= $task->merge_reg_staging ?></td>
            </tr>
            <tr>
              <td><strong>Merge_reg_live</strong></td>
              <td><?= $task->merge_reg_live ?></td>
            </tr>
          </tbody>
        </table>
      </div>
  </div>
</div>


<div id="wrapper">
    <div class="notification-ringi">
        <div class="clear"></div>
    </div>
    <fieldset class="fsStyle">
        <legend class="legendStyle">
            <?= __('List function change') ?>
            <div class="msg_func" style="display: inline; font-size: 15px;"></div>
            <span style="float: right;" >
                <a data-toggle="modal" data-target="#func_chitiet" data-title="add_func" data-member='<?= $task->id ?>'>
                    <span class="glyphicon glyphicon-plus"></span>
                </a>
            </span>
        </legend>
    <div id = "listfunc">
    <table class = "table table-striped table-bordered table-hover">
            <thead>
                <tr class="danger" >
                    <th style="width: 100px">Số thứ tự</th>
                    <th style="width: 183px">Task_ID</th>
                    <th style="width: 183px">Func</th>
                    <th style="width: 183px">Changes_type</th>
                    <th style="width: 183px">Description</th>
                    <th style="width: 183px">Note</th>
                    <th style="width: 183px">Active</th>
                </tr>
            </thead>

            <tbody>
            <?php
            $dem = 0;
                foreach ($functionchange as $key => $dataFunction) { 
                    if ($dataFunction->task_id == $task->id ) {
                    $dem++;?>
                            <tr class = "info"> 
                                <td style = "text-align: center;"><?= $dem ?></td>
                                <td id='<?= $dataFunction->id ?>'> <?= $dataFunction->task_id ?></td>
                                <td id="func<?= $dataFunction->id ?>"><?= $dataFunction->func ?></td>
                                <td id="<?= $dataFunction->id ?>"><?= $arrFuncType[$dataFunction->change_type] ?></td>
                                <td id="description_func<?= $dataFunction->id ?>"><?= $dataFunction->description ?></td>
                                <td id="note_func<?= $dataFunction->id ?>"><?= $dataFunction->note ?></td>
                                <td >
                                    <a class="btn btn-success" data-toggle="modal" data-target="#func_chitiet" data-title="edit_func" data-member='<?= $dataFunction->id ?>'>
                                            <span class="glyphicon glyphicon-pencil"></span>
                                    </a>
                                    <a class="btn btn-primary" onclick="deleteFunc(<?= $dataFunction->id ?>);">
                                            <span class="glyphicon glyphicon-trash"></span>
                                    </a>
                                </td>
                                <div style = "display:none;">
                                <input value = "<?= $dataFunction->change_type ?>"  id='change_type_func<?= $dataFunction->id ?>'> </input>
                            </tr>
                    <?php } ?>
                <?php } ?>
            </tbody>
    </table>
    </div>
    </fieldset>
</div>
<?= $this->element('add_function'); ?>



<div id="wrapper">
    <div class="notification-ringi">
        <div class="clear"></div>
    </div>
    <fieldset class="fsStyle">
        <legend class="legendStyle">
            <?= __('List process step') ?>
            <div class="msg_step" style="display: inline; font-size: 15px;"></div>
            <span style="float: right;" >
                <a data-toggle="modal" data-target="#step_chitiet" data-title="add_step" data-member='<?= $task->id ?>'>
                    <span class="glyphicon glyphicon-plus"></span>
                </a>
            </span>
        </legend>
    <div id = "liststep">
    <table class = "table table-striped table-bordered table-hover">
            <thead>
                <tr class="danger" >
                    <th style="width: 100px">Số thứ tự</th>
                    <th style="width: 183px">Task_ID</th>
                    <th style="width: 183px">Title</th>
                    <th style="width: 183px">Change_files</th>
                    <th style="width: 183px">Người thực hiện sửa</th>
                    <th style="width: 183px">Status</th>
                    <th style="width: 183px">Active</th>
                    
                </tr>
            </thead>

            <tbody>
            <?php
            $dem = 0;
                foreach ($processstep as $key => $dataProcessstep) { 
                    if ($dataProcessstep->task_id == $task->id ) {
                    $dem++;?>
                            <tr class = "info"> 
                                <td style = "text-align: center;"><?= $dem ?></td>
                                <td id='<?= $dataProcessstep->id ?>'> <?= $dataProcessstep->task_id ?></td>
                                <td id="title_step<?= $dataProcessstep->id ?>"><?= $dataProcessstep->title ?></td>
                                <td id="change_files<?= $dataProcessstep->id ?>"><?= $dataProcessstep->change_files ?></td>
                                <td id="<?= $dataProcessstep->id ?>"><?= $arrMembers1[$dataProcessstep->editor] ?></td>
                                <td id="<?= $dataProcessstep->id ?>"><?= $arrProcessStatus[$dataProcessstep->status] ?></td>

                                <td >
                                    <a class="btn btn-success" data-toggle="modal" data-target="#step_chitiet" data-title="edit_step" data-member='<?= $dataProcessstep->id ?>'>
                                            <span class="glyphicon glyphicon-pencil"></span>
                                    </a>
                                    <a class="btn btn-primary" onclick="deleteStep(<?= $dataProcessstep->id ?>);">
                                            <span class="glyphicon glyphicon-trash"></span>
                                    </a>
                                    
                                    
                                </td>
                                <div style = "display:none;">
                                <input value = "<?= $dataProcessstep->editor ?>"  id='editor<?= $dataProcessstep->id ?>'> </input>
                                <input value = "<?= $dataProcessstep->status ?>"  id='status<?= $dataProcessstep->id ?>'> </input>
                                <p id='description_step<?= $dataProcessstep->id ?>'> <?= $dataProcessstep->description ?> </p>
                                </div>
                            </tr>
                    <?php } ?>
                <?php } ?>
            </tbody>
    </table>
    </div>
    </fieldset>
</div>
<?= $this->element('add_step'); ?>




<div id="wrapper">
    <div class="notification-ringi">
        <div class="clear"></div>
    </div>
    <fieldset class="fsStyle">
        <legend class="legendStyle">
            <?= __('List database change') ?>
            <div class="msg_data" style="display: inline; font-size: 15px;"></div>
            <span style="float: right;">
                <a data-toggle="modal" data-target="#data_chitiet" data-title="add_data" data-member='<?= $task->id ?>'>
                    <span class="glyphicon glyphicon-plus"></span>
                </a>
            </span>
        </legend>
    <div id = "listdata">
    <table class = "table table-striped table-bordered table-hover">
            <thead>
                <tr class="danger" >
                    <th style="width: 100px">Số thứ tự</th>
                    <th style="width: 183px">Task_ID</th>
                    <th style="width: 183px">Table_name</th>
                    <th style="width: 183px">Change_type</th>
                    <th style="width: 183px">Queries</th>
                    <th style="width: 183px">Optimized</th>
                    <th style="width: 183px">Active</th>
                    
                </tr>
            </thead>

            <tbody>
            <?php
            $dem = 0;
                foreach ($databasechange as $key => $dataDatabase) { 
                    if ($dataDatabase->task_id == $task->id ) {
                    $dem++;?>
                            <tr class = "info"> 
                                <td style = "text-align: center;"><?= $dem ?></td>
                                <td id='<?= $dataDatabase->id ?>'> <?= $dataDatabase->task_id ?></td>
                                <td id="table_name<?= $dataDatabase->id ?>"><?= $dataDatabase->table_name ?></td>
                                <td id="<?= $dataDatabase->id ?>"><?= $arrFuncType[$dataDatabase->change_type] ?></td>
                                <td id="queries<?= $dataDatabase->id ?>"><?= $dataDatabase->queries ?></td>
                                <td id="<?= $dataDatabase->id ?>"><?= $arrDbOtimized[$dataDatabase->optimized] ?></td>
                                <td >
                                    <a class="btn btn-success" data-toggle="modal" data-target="#data_chitiet" data-title="edit_data" data-member='<?= $dataDatabase->id ?>'>
                                            <span class="glyphicon glyphicon-pencil"></span>
                                    </a>
                                    <a class="btn btn-primary" onclick="deleteDatabase(<?= $dataDatabase->id ?>);">
                                            <span class="glyphicon glyphicon-trash"></span>
                                    </a>
                                    
                                    
                                </td>
                                <div style = "display:none;">
                                <input value = "<?= $dataDatabase->change_type ?>"  id='change_type_data<?= $dataDatabase->id ?>'> </input>
                                <input value = "<?= $dataDatabase->optimized ?>"  id='optimized<?= $dataDatabase->id ?>'> </input>
                                <p id='note_data<?= $dataDatabase->id ?>'> <?= $dataDatabase->note ?> </p>
                                <p id='description_data<?= $dataDatabase->id ?>'> <?= $dataDatabase->description ?> </p>
                                </div>
                            </tr>
                    <?php } ?>
                <?php } ?>
            </tbody>
    </table>
    </div>
    </fieldset>
</div>
<?= $this->element('add_data'); ?>
