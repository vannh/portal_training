<div id="dialog-form-schedule" title="Add schedule">
    <div id = "msg_error_schedule"></div>
    <form name = "expect-schedule" method = "post" >
        <fieldset>
	  		<div class="form-group">
			    <label for="project">Project</label>
			    <input type="text" class="form-control" id="project" placeholder="Project Name">
	  		</div>
			<br>
			<div class="form-group">
			    <label for="deadline">DeadLine</label>
			    <input type="text" class="form-control" id="deadline" placeholder="DeadLine">
	  		</div>
			<br>
        </fieldset>
    </form>
</div>
<script>
    $('#deadline').datetimepicker({theme:'white'});
</script> 