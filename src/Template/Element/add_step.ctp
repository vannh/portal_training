<div id="step_chitiet" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2><?= __("Add Process step") ?></h2>
            </div>
            
            <div class="modal-body form-group">
            
                <?= $this->Form->create(null, array('id' => 'frmCreateTask', 'name' => 'frmCreateTask', 'horizontal' => true)); ?>
                    <?= $this->Form->input('', array('id' => 'id_step', 'type' => 'hidden', 'value' => 0)); ?>

                    <?= $this->Form->input(__('task_id'), array('label' =>__('Task_id'),'id' => 'task_id_step', 'type' => 'hidden', 'value' => '')); ?>

                    
                    <?= $this->Form->input(__('title'), array('label' =>__('Title'),'id' => 'title_step', 'type' => 'text', 'value' => '' , 'maxlength' => 255, 'class'=>'inputText3', 'placeholder'=>'Title', 'required')); ?>

                    <div class="invalid-msg"></div>

                    <?= $this->Form->input(__('change_files'), array('label' =>__('Change_files'),'id' => 'change_files', 'type' => 'text', 'value' => '' , 'maxlength' => 255, 'class'=>'inputText3', 'placeholder'=>'Change_files', 'required')); ?>

                    <div class="invalid-msg"></div>

                    <?= $this->Form->input(__('description'), array('label' =>__('Description'),'id' => 'description_step', 'type' => 'textarea', 'value' => '' , 'maxlength' => 255, 'class'=>'inputText3', 'placeholder'=>'Description', 'required')); ?>

                    <div class="invalid-msg"></div>


                    <div class="form-group text ">
                        <label class="col-md-2 control-label"><?= __("Editor") ?></label>
                          <div class="col-md-6">
                            <select  id = "editor" class="form-control alo3">
                            <option value = "0">Chọn người thực hiện</option>
                              <?php foreach($arrMembers1 as $key => $value) { ?>
                                <option value = "<?= $key ?>"><?= $value?></option>
                              <?php  debug($arrMembers1); } ?>
                            </select>
                          </div>
                    </div>
                    <div class="invalid-msg"></div>

                    <div class="form-group text ">
                        <label class="col-md-2 control-label"><?= __("Status") ?></label>
                          <div class="col-md-6">
                            <select  id = "status" class="form-control alo3">
                            <option value = "0">Chọn Status</option>
                              <?php foreach($arrProcessStatus as $key => $value) { ?>
                                <option value = "<?= $key ?>"><?= $value?></option>
                              <?php } ?>
                            </select>
                          </div>
                    </div>
                    <div class="invalid-msg"></div>
                   
                <?= $this->Form->end(); ?>
            <div class="msg"></div>
            </div>
           
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id = "myButton_step">Close</button>
                <button type="button" id="add_step" class="btn btn-primary">Save</button>
            </div>

        </div>
    </div>
</div>

<div id="dialog-confirm-step" class="dialog-confirm" title="Are you delele?">
    <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>These task will be permanently deleted. Are you sure?</p>
</div>