<div id="dialog-form-db" title="Add Query">
    <form name = "db-form" method = "post" >
        <fieldset>
          <div align = "center" style = "color:red; font-size: bold;" id="error_addReleaseDB"></div>
            <div class="form-group">
                <label for="table_db">Table Name</label>
                <input type="text" class="form-control" id="table_db" placeholder="Table Name">
            </div>
                <label>Editor</label>
            <div style = "display:none;"><input type = 'hidden' id = "id_db" value ="0" /></div>
            <select id = "editor_db" class="form-control">
            <?php
            foreach($Type_Database as $key => $value)
            { ?>
              <option value="<?= $key; ?>"><?= $Type_Database[$key]; ?></option>
            <?php
            }
            ?>
            </select>
            <br>
            <label>Status</label>
            <select class="form-control" id ="status_db">
            <?php
            foreach($Status_DB as $key => $value)
            { ?>
              <option value="<?= $key; ?>"><?= $Status_DB[$key]; ?></option>
            <?php
            }
            ?>
            </select>
            <br>
            <br>
            <label for="query">Query</label>
            <textarea name="query_db" id ="query" class="form-control" col= "3" rows="4"></textarea>
            <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
            <br>
        </fieldset>
    </form>
</div>