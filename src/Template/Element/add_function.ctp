<div id="func_chitiet" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2><?= __("Add Function change") ?></h2>
            </div>
            
            <div class="modal-body form-group">
            
                <?= $this->Form->create(null, array('id' => 'frmCreateTask', 'name' => 'frmCreateTask', 'horizontal' => true)); ?>
                    <?= $this->Form->input('', array('id' => 'id_func', 'type' => 'hidden', 'value' => 0)); ?>

                    <?= $this->Form->input(__('task_id'), array('label' =>__('Task_id'),'id' => 'task_id_func', 'type' => 'hidden', 'value' => '')); ?>

                    
                    <?= $this->Form->input(__('func'), array('label' =>__('Func'),'id' => 'func', 'type' => 'text', 'value' => '' , 'maxlength' => 255, 'class'=>'inputText2', 'placeholder'=>'Func', 'required')); ?>

                    <div class="invalid-msg"></div>


                    <div class="form-group text ">
                        <label class="col-md-2 control-label"><?= __("Change_type") ?></label>
                          <div class="col-md-6">
                            <select  id = "change_type_func" class="form-control alo2">
                            <option value = "0">Chọn Change_type</option>
                              <?php foreach($arrFuncType as $key => $value) { ?>
                                <option value = "<?= $key ?>"><?= $value?></option>
                              <?php } ?>
                            </select>
                          </div>
                    </div>
                    <div class="invalid-msg"></div>

                    <?= $this->Form->input(__('description'), array('label' =>__('Description'),'id' => 'description_func', 'type' => 'textarea', 'value' => '' , 'maxlength' => 255, 'class'=>'inputText2', 'placeholder'=>'Description', 'required')); ?>

                    <div class="invalid-msg"></div>


                    <?= $this->Form->input(__('note'), array('label' =>__('Note'),'id' => 'note_func', 'type' => 'textarea', 'value' => '' , 'maxlength' => 255, 'class'=>'inputText2', 'placeholder'=>'Note', 'required')); ?>

                    <div class="invalid-msg"></div>

                   
                   
                <?= $this->Form->end(); ?>
            <div class="msg"></div>
            </div>
           
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id = "myButton_func">Close</button>
                <button type="button" id="add_function_changes" class="btn btn-primary">Save</button>
            </div>

        </div>
    </div>
</div>

<div id="dialog-confirm-func" class="dialog-confirm" title="Are you delele?">
    <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>These task will be permanently deleted. Are you sure?</p>
</div>