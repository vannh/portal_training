<div id="dialog-form-expected" title="Add Expected">
    <div id = "msg_error_expected"></div>
    <form name = "expect-form" method = "post" >
        <fieldset>
	        <div class="form-group">
			    <label for="time">Times</label>
			    <input type="text" class="form-control" id="time" placeholder="Times GMT+7">
	  		</div>
	  		<br>	
	  		<div class="form-group">
			    <label for="action">Description</label>
			    <input type="text" class="form-control" id="action" placeholder="Description">
	  		</div>
	  		<br>
	  		<div style = "display:none;"><input type = 'hidden' id = "id_expected" value ="0" /></div>
	  		<label for="assignee">Assignee</label>
	  		<select name ="assignee" id = "assignee" class="form-control">
	  		<?php foreach($users as $key => $data)
    		{			 
    		?>
				<option value = "<?= $data['id'] ?>"><?= $data['full_name']; ?></option>
			<?php
			} 
			?>
			</select>
			<br>
			<label for="status_process">Status</label>
			<select name ="status_process" id = "status_process" class="form-control">
	  		<?php foreach($status_process as $key => $data)
    		{			 
    		?>
				<option value = "<?= $key ?>"><?= $data ?></option>
			<?php
			} 
			?>
			</select>
			<br>
			<label for="review">Review</label>
			<select name ="review" id = "review" class="form-control">
	  		<?php foreach($users as $key => $data)
    		{			 
    		?>
				<option value = "<?= $data['id'] ?>"><?= $data['full_name']; ?></option>
			<?php
			} 
			?>
			</select>
			<br>
			<label for="note">Comment</label>
			<textarea id = "note" class="form-control" rows="3"></textarea>
        </fieldset>
    </form>
</div>
<script>
    $('#time').datetimepicker({theme:'white'});
</script> 