<div id="dialog-form">
    <p class="validateTips"></p>
    <form name="myform" method="post">
        <fieldset>
            <label for="redmine_id">remine_id</label>
                <input  type="text" name="redmine_id" id="redmine_id"  class="text ui-widget-content ui-corner-all form-control">
            <label for="release_date">release_date</label>
                <input type="text" name="release_date" id="release_date"  class="form-control text ui-widget-content ui-corner-all">
            <label>user_release</label>
                <select class= "form-control" name="user_release" id = "user_release" >
                <?php foreach($users as $data){ ?>
                  <option value=" <?= $data->id; ?> "><?= $data->full_name; ?></option>
                <?php 
                } ?>
              </select>
              <br>
            <label>Has_Change_DB</label>
                <input class="checkbox" type="checkbox" id="inlineCheckbox1" name = "option1"> 
            </label>
            <br>
            <br>
            <label>DB_Backup</label>
                <input class="checkbox" type="checkbox" id="inlineCheckbox2" name = "option2">
            <br>
            <br>
            <label>Status</label>
                <select class="form-control" name="status" id = "status" >
                <?php 
                foreach($statusTest as $key => $value){ ?>
                    <option value= "<?= $key; ?>"><?= $value ?></option>
                <?php 
                }
                ?>
                </select>
                <div style = "display:none;"><input type = 'hidden' id = "releaseId" value ="0" />
                </div>
            <br>
            <label for="title_release">Title_Release</label>
            <textarea name="title_release" id ="title_release" class="form-control" col= "3" rows="4"></textarea>
            <input id = "submit" type="button" tabindex="-1" style="position:absolute; top:-1000px">
        </fieldset>
    </form>
</div>
<script>
    $('#release_date').datetimepicker({theme:'white'});
</script>