<div id="dialog-form-file" title="Add File">
    <form name = "file-form" method = "post" >
        <fieldset>
          <div align = "center" style = "color:red; font-size: bold;" id="error_add_file"></div>
            <div class="form-group">
                <label for="File_Path">File_Path</label>
                <input type="text" class="form-control" id="File_Path" placeholder="File_Path" maxlength="255" minlength="2">
            </div>
                <label>State</label>
            <div style = "display:none;"><input type = 'hidden' id = "id_file" value ="0" /></div>
            <select id = "state" class="form-control">
            <?php
            foreach($state as $key => $value)
            { ?>
              <option value="<?= $key; ?>"><?= $state[$key]; ?></option>
            <?php
            }
            ?>
            </select>
            <br>
            <br>
            <label>Coder</label>
            <select class="form-control" id ="coder">
            <?php foreach($users as $user) { ?>
                 <option value = "<?= $user['id']; ?>"><?= $user['full_name'];?></option>
            <?php
            } ?>
            </select>
            <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
            <br>
        </fieldset>
    </form>
</div>
  