<div align = "left" style = "margin-top:20px;"> <div class = "alert alert-info" style = "background:#34A853"> Schedule Task </div></div>
<div align = "right"><div class = "glyphicon glyphicon-tasks" id="addSchedule"></div>
<div id = "example5" >
<table class="table table-hover table-bordered">
    <thead>
        <tr>
            <th style = "background: #34A853;text-align: center; border:inset 1px black; width:10%;color:#fff;">STT</th>
            <th style = "background: #34A853;text-align: center; border:inset 1px black; width:30%;color:#fff;">Project</th>
            <th style = "background: #34A853;text-align: center; border:inset 1px black; width:40%;color:#fff;">DeadLine</th>
            <th style = "background: #34A853;text-align: center; border:inset 1px black; width:20%;color:#fff;">Action</th>
        </tr>
    </thead>
    <tbody>
    <?php $stt = 0;
    foreach($project as $data) {
    ?>
        <tr>
            <td style = "width:10%; vertical-align:middle;"><?= $stt++; ?></td>
            <td style = "width:30% ; vertical-align:middle; word-break:break-all;"><?= $data['name']; ?></td>
            <td  style = "width:40%;vertical-align:middle;"><?= $data['deadline']; ?></td>
            <td style = "width:20%;">
                <?= $this->Html->link((''),['action'=>'details',$data['id']],array('class' => 'glyphicon glyphicon-list-alt')); ?>        
                 <div style = " margin-left: 15px; margin-right:15px; ; cursor:pointer;" class="glyphicon glyphicon-trash" onclick=""></div>
            </td>
         </tr>
    <?php 
    } 
    ?>
    </tbody>
</table>
</div>

<?= $this->element('addSchedule'); ?>