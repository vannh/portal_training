<?php
namespace App\Controller;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\Config\AppConst;
use Cake\Controller\Component\FlashComponent;

class ReleasesController extends AppController
{
    public function index()
    {   
        $pagination_release =  Configure::read('pagination_release');
        $Users = TableRegistry::get('Users');
        $Releases = TableRegistry::get('Releases');
        $users = $Users->find()->toArray();
        $ele_release = $Releases->find('all')
        ->hydrate(false)
        ->join([
            'm' => [
                'table' => 'rpt_member',
                'type' => 'INNER',
                'conditions' => 'm.id = Releases.user_release',
                'limit' => '',
            ]
        ])
        ->select(['Releases.id','m.id', 'm.full_name','Releases.redmine_id','Releases.release_date','Releases.user_release','Releases.title_release','Releases.has_change_db','Releases.db_backup','Releases.status',])
        ->order(['Releases.id'=>'desc']);
        $this->paginate = ['limit' => $pagination_release,'order'=>['id'=>'desc']];
        $this->paginate($ele_release);
        $statusTest = Configure::read('statusTest');
        $this->set(compact('users','ele_release','statusTest'));
    }

   // public $paginate = ['rpt_releases'=>['limit' => $pagination_release,'order'=>['id'=>'desc']]];
    public function details($id = null)
    {
        $Users = TableRegistry::get('Users');
        $Tasks = TableRegistry::get('Tasks');
        $Release_Database = TableRegistry::get('ReleaseDatabase');
        $ele_release = $this->Releases->get($id);
        $db = Configure::read('db_change_and_backup');
        //data task selection
        $query_tmp = $Tasks->find('all')
        ->hydrate(false)
        ->join([
            'm' => [
                'table' => 'rpt_member',
                'type' => 'INNER',
                'conditions' => 'm.id = Tasks.assigned',
            ]
        ])
        ->select(['Tasks.id','Tasks.redmine_id', 'Tasks.title','m.full_name']);
        $this->set('data',$query_tmp);
        $query = $Tasks->find('all')
        ->hydrate(false)
        ->join([
            'r' => [
                'table' => 'rpt_release_task',
                'type' => 'INNER',
                'conditions' => 'r.task_id = Tasks.id',
            ],
            'm' => [
                'table' => 'rpt_member',
                'type' => 'INNER',
                'conditions' => 'm.id = Tasks.assigned',
            ]
        ])
        ->select(['Tasks.redmine_id', 'Tasks.title','m.full_name','r.id'])
        ->order(['r.id'=>'desc'])
        ->where(['r.release_id' => $id]);   
        $this->set('tmp',$query);
        $status = Configure::read('statusTest');
        $state = Configure::read('state');
        $query_file = $this->Releases->find('all')
        ->hydrate(false)
        ->join([
            'f' => [
                'table' => 'rpt_release_files',
                'type' => 'INNER',
                'conditions' => ' f.release_id = Releases.id',
            ],
            'm' => [
                'table' => 'rpt_member',
                'type' => 'INNER',
                'conditions' => 'm.id = f.coder',
            ]
        ])
        ->select(['Releases.id','f.id','f.file_path','f.state','f.coder', 'm.full_name'])
        ->order(['f.id'=>'desc'])
        ->where(['f.release_id' => $id]); 
        $this->set('data_file',$query_file);
        $users = $Users->find()->toArray();
        $query_database = $this->Releases->find('all')
        ->hydrate(false)
        ->join([
            'd' => [
                'table' => 'rpt_release_database',
                'type' => 'INNER',
                'conditions' => ' d.release_id = Releases.id'
            ]
        ])
        ->select(['Releases.id','d.id','d.editor','d.query_db', 'd.table_db','d.status'])
        ->order(['d.id'=>'desc'])
        ->where(['d.release_id' => $id]);
        $this->set('database_data',$query_database);
        $Status_DB = Configure::read('Status_Change_Database');
        $Type_Database = Configure::read('Type_Change_Database');
        $query_expected = $this->Releases->find('all')
        ->hydrate(false)
        ->join([
            'e' => [
                'table' => 'rpt_release_expected',
                'type' => 'INNER',
                'conditions' => ' e.release_id = Releases.id'
            ],
            'm' => [
                'table' => 'rpt_member',
                'type' => 'LEFT',
                'conditions' => 'm.id = e.assignee',
            ],
            'tmp' => [
                'table' => 'rpt_member',
                'type' => 'LEFT',
                'conditions' => 'tmp.id = e.review',
            ],
        ])
        ->select(['Releases.id','e.id','e.time_expected','e.action','tmp.full_name','m.full_name','e.assignee','e.status','e.review','e.note'])
        ->order(['e.id'=>'desc'])
        ->where(['e.release_id' => $id]);
        $status_process = Configure::read('status_process');
                $this->set(compact('users','status','db','ele_release','state','status_process','query_expected','Status_DB','Type_Database'));
    }

    public function addRelease()
    {   
        $arrayData = array();
        $arrReturn = array();
        $arrayData = $this->request->data;
        $test = $this->validate($arrayData);
         $query =  $this->Releases->find('all')->where(['redmine_id' => trim($arrayData['redmine_id']),'id !=' => $arrayData['id']])->toArray();
        if ($test != ""){
            $arrReturn = array("status" => false, "text" => $test);
        } else {
                if(!empty($query)){
                $arrReturn = array("status" => false, "msg" => "Your Release Are Duplicate . Please, Try Again.");
                } else {
                $release = $this->Releases->newEntity();
                $id = $this->request->data['id'];
                if ($id == 0){
                    $date_now = date("Y-m-d H:i:s");
                    $release->created = $date_now;
                    $release->modified = $date_now;
                } else {
                    $date_now = date("Y-m-d H:i:s");
                    $release->modified = $date_now;
                }
                if ($this->request->is('post')){
                    $release = $this->Releases->patchEntity($release, $arrayData);
                    if ($this->Releases->save($release)){
                        $arrReturn = array( "status"=>true,"msg" => "Your Release has been saved."); 
                    }
                    else{
                        $arrReturn = array("status" => false, "msg" => "Your Release could not be saved. Please, try again.");
                                }
                    }
                }
        }
        echo json_encode($arrReturn);
        exit;
    }

    public function addReleaseTask()
    {
        $arrayData = array();
        $arrReturn = array();
        $arrayData = $this->request->data;
        $query =  $this->ReleaseTask->find('all')->where(['release_id'=> $arrayData['release_id'],'task_id' => $arrayData['task_id']])->toArray();
        if(!empty($query)){
             $arrReturn = array("status" => false, "msg" => "Your Task Are Duplicate . Please, Try Again.");
        } else {
            $task = $this->ReleaseTask->newEntity();
            $date_now = date("Y-m-d H:i:s");
            $task->created = $date_now;
            $task->modified = $date_now;
            if ($this->request->is('post')){
                $task = $this->ReleaseTask->patchEntity($task,$this->request->data);
                if ($this->ReleaseTask->save($task)){
                    $arr = $arrReturn = array( "status"=>true,"msg" => "Your Task has been saved."); 
                } else {
                        $arrReturn = array("status" => false, "msg" => "Your Task could not be saved. Please, try again.");
                }
            }
        }
        echo json_encode($arrReturn);
        exit;
    }

    public function addReleaseFile()
    {

        $arrayData = array();
        $arrReturn = array();
        $arrayData = $this->request->data;
        $test = $this->validate_file($arrayData);
         $query =  $this->ReleaseFiles->find('all')->where(['release_id'=> $arrayData['release_id'],'file_path' => trim($arrayData['file_path']),'coder' => $arrayData['coder'],'id !=' => $arrayData['id']])->toArray();
        if ($test != ""){
            $arrReturn = array("status" => false, "text" => $test);
        } else{
                if(!empty($query)){
                    $arrReturn = array("status" => false, "msg" => "Your File_Path Are Duplicate . Please, Try Again.");
                } else {
                    $file = $this->ReleaseFiles->newEntity();
                    $id = $this->request->data['id'];
                    if ($id == 0){
                        $date_now = date("Y-m-d H:i:s");
                        $file->created = $date_now;
                        $file->modified = $date_now;
                    }else{
                        $date_now = date("Y-m-d H:i:s");
                        $file->modified = $date_now;
                    }
                    if ($this->request->is('post')){
                        $file = $this->ReleaseFiles->patchEntity($file, $arrayData);
                        if ($this->ReleaseFiles->save($file)){
                            $arrReturn = array( "status"=>true,"msg" => "Your File has been saved."); 
                        } else {
                            $arrReturn = array("status" => false, "msg" => "Your File_Path could not be saved. Please, try again.");
                                    }
                        }
                }
        }
        echo json_encode($arrReturn);
        exit;
    }

    public function addReleaseDB()
    {
        $arrayData = array();
        $arrReturn = array();
        $arrayData = $this->request->data;
        $test = $this->validate_db($arrayData);
        $query =  $this->ReleaseDatabase->find('all')->where(['release_id'=> $arrayData['release_id'],'query_db' => trim($arrayData['query_db']),'table_db' => trim($arrayData['table_db']),'id !=' => $arrayData['id']])->toArray();
        if ($test != ""){
             $arrReturn = array("status" => false, "text" => $test);
        } else {
                if(!empty($query)){
                    $arrReturn = array("status" => false, "msg" => "Your option Are Duplicate . Please, Try Again.");
                } else {
                    $db = $this->ReleaseDatabase->newEntity();
                    $id = $this->request->data['id'];
                    if ($id == 0){
                        $date_now = date("Y-m-d H:i:s");
                        $db->created = $date_now;
                        $db->modified = $date_now;
                    } else {
                        $date_now = date("Y-m-d H:i:s");
                        $db->modified = $date_now;
                    }
                    if ($this->request->is('post')){
                        $db = $this->ReleaseDatabase->patchEntity($db, $arrayData);
                        if($this->ReleaseDatabase->save($db)){
                            $arrReturn = array( "status"=>true,"msg" => "Your option has been saved."); 
                        }
                        else{
                            $arrReturn = array("status" => false, "msg" => "Your option could not be saved. Please, try again.");
                        }   
                     }
                 }
        }
        echo json_encode($arrReturn);
        exit;
    }

    public function addReleaseExpected()
    {
        $this->loadModel('ReleasesExpected');
        $arrayData = array();
        $arrReturn = array();
        $arrayData = $this->request->data;
        $test = $this->validate_expected($arrayData);
        if ($test != ""){
             $arrReturn = array("status" => false, "text" => $test);
        } else {
            $ex = $this->ReleasesExpected->newEntity();
            $id = $this->request->data['id'];
            if ($id == 0){
                $date_now = date("Y-m-d H:i:s");
                $ex->created = $date_now;
                $ex->modified = $date_now;
            }else{
                $date_now = date("Y-m-d H:i:s");
                $ex->modified = $date_now;
            }
            if ($this->request->is('post')){
               // debug($this->request->is('post'));
                $ex = $this->ReleasesExpected->patchEntity($ex, $arrayData);
             //   debug($ex);
                if($this->ReleasesExpected->save($ex)){
                    $arrReturn = array( "status"=>true,"msg" => "Your option has been saved."); 
                }
                else{
                    $arrReturn = array("status" => false, "msg" => "Your option could not be saved. Please, try again.");
                }   
             }
        }
        echo json_encode($arrReturn);
        exit;
    }

    public function DeleteReleaseDB(){
        $id = $this->request->data['id'];
        $query =  $this->ReleaseDatabase->find('all')->where(['id'=> $id])->toArray();
        if(empty($query))
        {
            $arrReturn = array("status" => false, "msg" => "Your Option has been deleted duplicate.");
        }
        else{
            $db = $this->ReleaseDatabase->get($id);
            if ($this->ReleaseDatabase->delete($db)) {
                $arrReturn = array("status" => true, "msg" => "Your Option has been deleted.");
            } else {
                $arrReturn = array("status" => false, "msg" => "Your Option could not be deleted. Please, try again.");
            }
        }
        echo json_encode($arrReturn); 
        exit;

    }

    public function DeleteReleaseExpected(){
        $id = $this->request->data['id'];
        $query =  $this->ReleasesExpected->find('all')->where(['id'=> $id])->toArray();
        if(empty($query))
        {
            $arrReturn = array("status" => false, "msg" => "Your Option has been deleted duplicate.");
        } else {
                $ex = $this->ReleasesExpected->get($id);
                if ($this->ReleasesExpected->delete($ex)) {
                    $arrReturn = array("status" => true, "msg" => "Your Option has been deleted.");
                } else {
                    $arrReturn = array("status" => false, "msg" => "Your Option could not be deleted. Please, try again.");
                }      
        }
        echo json_encode($arrReturn); 
        exit;

    }

    public function deleteReleaseTask()
    {
        $id = $this->request->data['id'];
        $query =  $this->ReleaseTask->find('all')->where(['id'=> $id])->toArray();
        if(empty($query))
        {
            $arrReturn = array("status" => false, "msg" => "Your Task has been deleted duplicate.");
        } else {
            $task = $this->ReleaseTask->get($id);
            if ($this->ReleaseTask->delete($task)) {
                $arrReturn = array("status" => true, "msg" => "Your Task has been deleted.");
            } else {
                $arrReturn = array("status" => false, "msg" => "Your Task could not be deleted. Please, try again.");
            }
        }
        echo json_encode($arrReturn); 
        exit;
    }

    public function DeleteReleaseFile()
    {
        $id = $this->request->data['id'];
        $query =  $this->ReleaseFiles->find('all')->where(['id'=> $id])->toArray();
        if(empty($query))
        {
            $arrReturn = array("status" => false, "msg" => "Your File has been deleted duplicate.");
        } else {
                $file = $this->ReleaseFiles->get($id);
                if ($this->ReleaseFiles->delete($file)) {
                    $arrReturn = array("status" => true, "msg" => "Your File has been deleted");
                } else {
                    $arrReturn = array("status" => false, "msg" => "Your File could not be deleted. Please, try again.");
                }
        }
        echo json_encode($arrReturn); 
        exit;
    }

    public function deleteRelease()
    {
        $id = $this->request->data['id'];
        $query =  $this->Releases->find('all')->where(['id'=> $id])->toArray();
        if(empty($query))
        {
            $arrReturn = array("status" => false, "msg" => "Your Release has been deleted duplicate.");
        } else {
                $file = $this->Releases->get($id);
                $release_del =  $this->Releases->get($id);
                if($this->Releases->delete($release_del))
                {
                    $this->ReleaseTask->deleteAll(['release_id' => $id]);
                    $this->ReleaseFiles->deleteAll(['release_id' => $id]);
                    $this->ReleaseDatabase->deleteAll(['release_id' => $id]);
                    $this->ReleasesExpected->deleteAll(['release_id' => $id]);
                    $arrReturn = array("status" => true, "msg" => "Your Release has been deleted");
                } else {

                     $arrReturn = array("status" => false, "msg" => "Your Release could not be deleted. Please, try again.");
                }
        }
       
        echo json_encode($arrReturn); 
        exit;
    }

    public function validate($data)
    {
        $msg = "";
        if(!preg_match("/^[0-9]{1,10}+$/i", $data["redmine_id"])){
            $msg .=' redmine_id must be Number between 1 and 10.';
        }
        if(empty($data["release_date"])){ 
            $msg .='You must add data in Releases date.';
        }
        if(empty(trim($data["title_release"])) || strlen($data["redmine_id"]) > 500){
            $msg .='Length of title_release must be between 1 and 500.';
        }
        return $msg;
    }

    public function validate_file($data)
    {
        $msg = "";
        if(!preg_match("/^[a-zA-Z0-9-._\/\\\]{1,255}+$/i", $data["file_path"])){
            $msg= "File Path Not Valid";
        }
        return $msg;
    }

    public function validate_db($data)
    {
        $msg = "";
        if(empty(trim($data["table_db"])) || strlen($data["table_db"]) > 50){
            $msg .='Length of Table Name must be between 1 and 50.';
        }
        if(empty(trim($data["query_db"]))){
            $msg .='You must add data in Query';
        }
        return $msg;
    }

    public function validate_expected($data)
    {
        $msg = "";
        if(empty(trim($data["action"])) || strlen($data["action"]) > 255){
            $msg .='Length of Table Name must be between 1 and 255. ';
        }
        if(empty($data["time_expected"])){
            $msg .='You must add data in Time Field ';
        } 
        return $msg;
    }
}