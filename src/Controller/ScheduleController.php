<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\Config\AppConst;
use Cake\Controller\Component\FlashComponent;

class ScheduleController extends AppController
{
    public function index()
    {
        $project = $this->Projects->find()->toArray();
        $this->set(compact('project'));
    }

    public function addSchedule()
    {
        $this->loadModel('Projects');
        $arrayData = array();
        $arrReturn = array();
        $arrayData = $this->request->data;
        $Projects = $this->Projects->newEntity();
        if ($this->request->is('post')){
            $Projects = $this->Projects->patchEntity($Projects, $arrayData);
            if($this->Projects->save($Projects)){
                $arrReturn = array( "status"=>true,"msg" => "Your option has been saved"); 
            }
            else{
                $arrReturn = array("status" => false, "msg" => "Your option could not be saved. Please, try again.");
            }   
        }
        echo json_encode($arrReturn);
        exit;
    }

    public function details($id = null)
    {
        $this->loadModel('Projects');
        $this->loadModel('Users');
        $this->loadModel('Calendar');
        $Calendar = TableRegistry::get('Calendar');
        $ele_project = $this->Projects->get($id);
        $this->set(compact('ele_project'));
        $users = $this->Users->find()->toArray();
        $this->set(compact('users'));
        $query = $Calendar->find('all')
        ->hydrate(false)
        ->join([
            'm' => [
                'table' => 'rpt_member',
                'type' => 'INNER',
                'conditions' => 'm.id = Calendar.coder',
            ],
            'p' => [
                'table' => 'rpt_projects',
                'type' => 'INNER',
                'conditions' => 'p.id = Calendar.project_id',
            ]
        ])
        ->select(['m.full_name','m.style'])
        ->where(['Calendar.project_id' => $id])
        ->group(['Calendar.coder']);
        $this->set(compact('query'));
    }
}   