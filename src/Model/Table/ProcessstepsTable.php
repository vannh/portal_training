<?php
namespace App\Model\Table;

use Cake\ORM\Table;

class ProcessstepsTable extends Table
{
    public function initialize(array $config)
    {
        $this->table('rpt_process_step');
    }
}

?>