<?php
namespace App\Model\Table;

use Cake\ORM\Table;

class FunctionchangesTable extends Table
{
    public function initialize(array $config)
    {
        $this->table('rpt_function_changes');
    }
}

?>