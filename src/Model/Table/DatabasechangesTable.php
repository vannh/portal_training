<?php
namespace App\Model\Table;

use Cake\ORM\Table;

class DatabasechangesTable extends Table
{
    public function initialize(array $config)
    {
        $this->table('rpt_database_changes');
    }
}

?>