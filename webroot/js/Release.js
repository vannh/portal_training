$(document).ready(function(){
    var dialog, form,
    redmine_id = $( "#redmine_id" ), // Lấy đối tượng có id
    release_date = $( "#release_date" ),
    title_release = $( "#title_release" ),
    user_release = $("#user_release"),
    allFields = $( [] ).add( redmine_id ).add( release_date ).add(user_release).add( title_release ),
    tips = $( ".validateTips" );
    function updateTips( t ){// animation của validate form bằng jquery với tham số truyền vào là 1 chuỗi
        tips
            .text( t )
            .addClass( "ui-state-highlight" );
            setTimeout(function() {
            tips.removeClass( "ui-state-highlight", 5000 );
            });
    }
    function checkLength( o, n, min, max ) {//check lenght xâu param là đối tượng có id o,chuỗi n báo lỗi,min và max lenght
        if ( o.val().length > max || o.val().length < min ) {
            o.addClass( "ui-state-error" );
            updateTips( "Length of " + n + " must be between " + min + " and " + max + "." );
            return false;
        }else{
            return true;
        } 
    }
    function checknull( o, n) { //check null
        if ( o.val().length == 0) {
            o.addClass( "ui-state-error" );
            updateTips( "You must add data in " +n );
            return false;
        }else{
            return true;
        }
    }
    function checkRegexp( o, regexp, n ) {
      if ( !( regexp.test( o.val() ) ) ) {
        o.addClass( "ui-state-error" );
        updateTips( n );
        return false;
      } else {
        return true;
      }
    }
    function addRelease() {// hàm add releases
        var valid = true;
        var releaseId = $('#releaseId').val();
        valid = valid && checkLength( redmine_id, "redmine_id", 1, 10 );
        valid = valid && checkRegexp( redmine_id, /^[0-9]+$/i, "This Fields Must Numbers 0-9" );
        valid = valid && checknull( release_date, "releases_date");
        valid = valid && checkLength( title_release, "title_release", 1, 500 );
        if ( valid ) 
        {
          var check1,check2 = 0;
          if($('#inlineCheckbox1').is(":checked")==true){
              check1 = '1';
          } else {
              check1 = '0';
          }
          if($('#inlineCheckbox2').is(":checked")==true){ 
              check2 = '1';
          } else {
              check2 = '0';
          }
          $.ajax({
              beforeSend: function(){//Sự kiện được kích hoạt trươc khi một Ajax request được bắt đầu
              // trước khi truyền data được bắn lên là data{} với post cho bên controller và được trả về 1 chuỗi json
              },  
              delay:0,
              url: '/releases/addRelease',
              type: 'POST',
              dataType: 'json',
              data: {"id":releaseId, "redmine_id": $("#redmine_id").val(),"release_date":$("#release_date").val(),"user_release":$("#user_release").val(),"title_release":$("#title_release").val(),"has_change_db":check1,"db_backup":check2,"status":$("#status").val()},
              success: function(data){ // khi thành công nhân được data msg
                        if(data.status == true){
                            if(releaseId!=0){
                                alert("Your Release Has Been Edit");
                            }else {
                                alert(data.msg);
                            }
                            dialog.dialog( "close" );
                            window.location.reload(true);
                        }else {
                             $('#msg_error').val((updateTips(data.text)));
                              updateTips(data.msg);
                        }
                      }
            });
        } 
    }
    dialog = $( "#dialog-form" ).dialog({// hộp đialog
        autoOpen: false,
        height: 500,
        width: 600,
        modal: true,
        buttons: {
        "Save ": addRelease,
        Cancel: function() {
                dialog.dialog( "close" );
            }
        },
        close: function(){
            form[ 0 ].reset();
            allFields.removeClass( "ui-state-error" );
        },
        hide: {
                effect: "explode",
                duration: 1000
      } 
    });
    form = dialog.find( "form" ).on( "submit", function( event ) {
        event.preventDefault();//dùng để chặn hành động mặc định của sự kiện, ví dụ như khi ta bấm vào thẻ a thì trình duyệt sẽ chuyển sang trang mới hoặc gửi nội dung của input khi form xảy ra sự kiện submit…
        addRelease();
    });
    $( "#create-user" ).button().on( "click", function() {
        $("#releaseId").val(0);
        $(".ui-dialog-title").html("Create New Release");
         dialog.dialog( "open" );
         allFields.removeClass( ".ui-state-error" );
        $('.validateTips').html('');
         dialog.effect( "slide", "slow" );
    });
});

function editRelease(id)
{
    $("#style").val(1);
    $(".ui-dialog-title").html("Edit Release");
    $("#releaseId").val(id);
    var redmine_id = $("#redmine_id" + id).html();
    $("#redmine_id").val(redmine_id);
    var release_date = $("#release_date" + id).html();
    $("#release_date").val(release_date);
    var inlineCheckbox1 = $('#inlineCheckbox1'+id).val();
    if(inlineCheckbox1==1){
        $('#inlineCheckbox1').prop('checked',true);
    }
    var inlineCheckbox2 = $('#inlineCheckbox2'+id).val();
    if(inlineCheckbox2==1){
        $('#inlineCheckbox2').prop('checked',true);
    }
    var user_release = $("#user_release" + id).val();
    $("#user_release").val(user_release);
    var status = $("#status" + id).val();
    $("#status").val(status);
    var title_release = $("#title_release" + id).val();
    $("#title_release").val(title_release);
    $( "#dialog-form" ).dialog("open");
    allFields.removeClass( ".ui-state-error" );
    $('.validateTips').html('');
    $( "#dialog-form" ).effect( "slide", "slow" );
}

function deleteRelease(id) {
    $( "#dialog-confirm-deleteRelease" ).dialog({
        height:250,
        width:350,
        modal: true,
        buttons: {
            "Delete": function() {
                $.ajax({
                    beforeSend: function(){
                    },
                    delay: 0,
                    url: '/releases/deleteRelease',
                    type: 'POST',
                    dataType: 'json',
                    data : { 'id' : id },
                    success: function(data){
                        if(data.status == false){
                            alert(data.msg)
                        }
                        window.location.reload(true);
                    }
                });
            },
            Cancel: function() {
                $( this ).dialog( "close" );
            }
        },
        hide: {
            effect: "explode",
            duration: 500
      }
    });
} 
