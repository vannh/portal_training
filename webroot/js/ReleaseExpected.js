$(document).ready(function(){
    var dialog_ex, form,
    time = $( "#time" ), // Lấy đối tượng có id
    action = $( "#action" ),
    note = $("#note"),
    allFields = $( [] ).add( time ).add( action ),
    tips = $( "#msg_error_expected" );
    function updateTips( t ){// animation của validate form bằng jquery với tham số truyền vào là 1 chuỗi
        tips
            .text( t )
            .addClass( "ui-state-highlight" );
            setTimeout(function() {
            tips.removeClass( "ui-state-highlight", 5000 );
            });
    }

    function checkLength( o, n, min, max ) {//check lenght xâu param là đối tượng có id o,chuỗi n báo lỗi,min và max lenght
        if ( o.val().length > max || o.val().length < min ) {
            o.addClass( "ui-state-error" );
            updateTips( "Length of " + n + " must be between " + min + " and " + max + "." );

            return false;
        }else{
            return true;
        } 
    }

    function checknull( o, n) { //check null
        if (!o.val()) {
            o.addClass( "ui-state-error" );
            updateTips( "You must add data in " +n );
            return false;
        }else{
            return true;
        }
    }

    function addReleaseExpected(){
        var valid = true;
        var id_exptected = $('#id_expected').val();
        var release_id = $('#release_id').val();
        valid = valid && checknull( time, "Time Field");
        valid = valid && checkLength( action, "Description", 1, 255 );
        if ( valid ) 
        {
          $.ajax({
              beforeSend: function(){//Sự kiện được kích hoạt trươc khi một Ajax request được bắt đầu
              // trước khi truyền data được bắn lên là data{} với post cho bên controller và được trả về 1 chuỗi json
              },  
              delay:0,
              url: '/releases/addReleaseExpected',
              type: 'POST',
              dataType: 'json',
              data: { "id": id_exptected,"status": $('#status_process').val(), "release_id": release_id, "time_expected":time.val() , "action": action.val(),"assignee":$('#assignee').val(), "review":$('#review').val(),"note":note.val()},
              success: function(data){ // khi thành công nhân được data msg
                        if(data.status == true){
                            if(id_exptected!=0){
                                alert("Your Option Has Been Edit");
                            }
                            else{
                                alert(data.msg);
                            }
                            $("#example4").load(location.href + " #example4");
                            dialog_ex.dialog( "close" );
                            }
                            else{
                                  $('#msg_error_expected').val((updateTips(data.text)));
                                  $('#msg_error_expected').addClass( "ui-state-error" );
                                  updateTips(data.msg);
                            }
                     }
                })
        } 
    }
    dialog_ex = $( "#dialog-form-expected" ).dialog({
        autoOpen: false,
        height: 600,
        width: 700,
        modal: true,
        buttons: {
            "Save Expected": addReleaseExpected,
            Cancel: function() {
              dialog_ex.dialog( "close" );
            }
        },
            close: function() {
              form_ex[0].reset();
            },
            hide: {
                effect: "explode",
                duration: 1000
      } 
    });
    form_ex = dialog_ex.find( "form" ).on( "submit", function( event ) {
        event.preventDefault();
        addReleaseExpected();
    });   
    $( "#add_expected" ).button().on( "click", function() {
        $(".ui-dialog-title").html("Add Expected");
        $("#id_expected").val(0);
        dialog_ex.dialog( "open" );
        $('#msg_error_expected').html('');
        allFields.removeClass( "ui-state-error" );
        allFields.removeClass( "msg_error_expected" );
        dialog_ex.effect( "slide", "slow" );
    });
});
function EditReleaseExpected(id)
{
    $(".ui-dialog-title").html("Edit Expected");
    $("#id_expected").val(id);
    var time = $("#time" + id).html();
    $("#time").val(time);
    var action = $("#action" + id).html();
    $("#action").val(action);
    var assignee = $("#assignee" + id).val();
    $("#assignee").val(assignee);
    var status_process = $("#status_process" + id).val();
    $("#status_process").val(status_process);
    var review = $("#review" + id).val();
    $("#review").val(review);
    var note = $("#note" + id).html();
    $("#note").val(note);
    $( "#dialog-form-expected" ).dialog("open");
    $('#msg_error_expected').html('');
    allFields.removeClass( "ui-state-error");
    allFields.removeClass( "msg_error_expected" );
    dialog_ex.effect( "slide", "slow" );
    $( "#dialog-form-expected" ).effect( "slide", "slow" );
}
function DeleteReleaseExpected(id) {
    $( "#dialog-confirm-expected" ).dialog({
        height:250,
        width:350,
        modal: true,
        buttons: {
            "Delete": function() {
                $.ajax({
                    beforeSend: function(){
                    },
                    delay: 0,
                    url: '/releases/DeleteReleaseExpected',
                    type: 'POST',
                    dataType: 'json',
                    data : { 'id' : id },
                    success: function(data){
                        if(data.status == false){
                            alert(data.msg);
                        }
                        $( "#dialog-confirm-expected" ).dialog( "close" );
                        $("#example4").load(location.href + " #example4");
                    }
                });
            },
            Cancel: function() {
                $( this ).dialog( "close" );
            }
        },
        hide: {
                effect: "explode",
                duration: 1000
      } 
    });
}

function show_expected(id) {
    $( "#dialog-show-expected" +id ).dialog({
        autoOpen: true,
        height:550,
        width:800,
        modal: true,
        hide: {
            effect: "explode",
            duration: 500
      },
    });
} 
