$(document).ready(function(){
   var dialog_schedule,
    deadline = $('#deadline'),
    allFields = $( [] ).add(deadline),
    tips = $( "#msg_error_schedule" );
    function updateTips( t ){// animation của validate form bằng jquery với tham số truyền vào là 1 chuỗi
        tips
            .text( t )
            .addClass( "ui-state-highlight" );
            setTimeout(function() {
            tips.removeClass( "ui-state-highlight", 5000 );
            });
    }

    function checknull( o, n) { //check null
        if (!o.val()) {
            o.addClass( "ui-state-error" );
            updateTips( "You must add data in " +n );
            return false;
        }else{
            return true;
        }
    }
    function add_Schedule(){  
        var valid = true;
        var project = $("#project").val();
        allFields.removeClass( "ui-state-error" );
        valid = valid && checknull( $("#project"), "Project" );
        valid = valid && checknull( deadline, "DeadLine" );
        if(valid){
            $.ajax({
                beforeSend : function(){},//Sự kiện được kích hoạt trươc khi một Ajax request được bắt đầu
                delay:0,
                url: '/schedule/addSchedule',
                type: 'POST',
                dataType: 'json',
                data: {"deadline": deadline.val(), "name": project},
                success: function(data){
                    if(data.status == true){
                        $("#example5").load(location.href + " #example5");
                        dialog_schedule.dialog( "close" );
                    }
                    else{
                      $('#msg_error_schedule').val((updateTips(data.text)));
                      $('#msg_error_schedule').addClass( "ui-state-error" );
                      updateTips(data.msg);
                    }
                }
            });
        }
    }
    dialog_schedule = $( "#dialog-form-schedule" ).dialog({
        autoOpen: false,
        height: 400,
        width: 800,
        modal: true,
        buttons: {
            "Save Schedule": add_Schedule,
            Cancel: function() {
              dialog_schedule.dialog( "close" );
            }
        },
            close: function() {
              form_schedule[0].reset();
            },
    hide: {
                effect: "explode",
                duration: 1000
      }  
    });
    form_schedule = dialog_schedule.find( "form" ).on( "submit", function( event ) {
        event.preventDefault();
        add_Schedule();
   });
   $( "#addSchedule" ).button().on( "click", function() {
        dialog_schedule.dialog( "open" );
        dialog_schedule.effect( "slide", "slow" );
    });   
});