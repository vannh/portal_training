$(document).ready(function(){
    var dialog_task;
    function addReleaseTask(){
          var task_id = $('#releasesTask').val();
          var release_id = $('#release_id').val();
          $.ajax({
              beforeSend : function(){
              },//Sự kiện được kích hoạt trươc khi một Ajax request được bắt đầu
              delay:0,
              url: '/releases/addReleaseTask',
              type: 'POST',
              dataType: 'json',
              data: {"task_id" : task_id , "release_id" : release_id},
              success: function(data){
                  if(data.status == true){
                      alert(data.msg);
                      $("#example1").load(location.href + " #example1");// phải cách ra 1 đoạn  " #example1"
                      dialog_task.dialog( "close" );
                  }
                  else{
                    $('#msg_error1').html(data.msg);
                  }
              }
          });
      }
    dialog_task = $( "#dialog-form-task" ).dialog({
        autoOpen: false,
        height: 300,
        width: 300,
        modal: true,
        buttons: {
            "Add Task": addReleaseTask,
            Cancel: function() {
              dialog_task.dialog( "close" );
            }
        },
            close: function() {
              form_task[0].reset();
            },
             hide: {
                effect: "explode",
                duration: 1000
      }  
    });
    form_task = dialog_task.find( "form" ).on( "submit", function( event ) {
        event.preventDefault();
        addReleaseTask();
    });   
    $( "#addTask" ).button().on( "click", function() {
        dialog_task.dialog( "open" );
        $('#msg_error1').html('');
        dialog_task.effect( "slide", "slow" );
    });
});

function deleteReleaseTask(id) {
    $( "#dialog-confirm-Releasetask" ).dialog({
        height:250,
        width:350,
        modal: true,
        buttons: {
            "Delete": function() {
                $.ajax({
                    beforeSend: function(){
                    },
                    delay: 0,
                    url: '/releases/deleteReleaseTask',
                    type: 'POST',
                    dataType: 'json',
                    data : { 'id' : id },
                    success: function(data){
                        if(data.status == false){
                            alert(data.msg);
                        }
                        $("#example1").load(location.href + " #example1");
                        $( "#dialog-confirm-Releasetask" ).dialog( "close" );   
                    }
                });
            },
            Cancel: function() {
                $( this ).dialog( "close" );
            }
        },
        hide: {
            effect: "explode",
            duration: 500
      }
    });
} 