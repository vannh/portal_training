$(document).ready(function(){
  var dialog_db,
  table_db = $('#table_db'),
  query = $('#query'),
  allFields = $( [] ).add( table_db ).add(query),
  tips = $( "#error_addReleaseDB" );
    function updateTips( t ){// animation của validate form bằng jquery với tham số truyền vào là 1 chuỗi
        tips
            .text( t )
            .addClass( "ui-state-highlight" );
            setTimeout(function() {
            tips.removeClass( "ui-state-highlight", 5000 );
            });
    }

    function checkLength( o, n, min, max ) {//check lenght xâu param là đối tượng có id o,chuỗi n báo lỗi,min và max lenght
        if ( o.val().length > max || o.val().length < min ) {
            o.addClass( "ui-state-error" );
            updateTips( "Length of " + n + " must be between " + min + " and " + max + "." );

            return false;
        }else{
            return true;
        } 
    }

    function checknull( o, n) { //check null
        if (!o.val()) {
            o.addClass( "ui-state-error" );
            updateTips( "You must add data in " +n );
            return false;
        }else{
            return true;
        }
    }

    function addReleaseDB(){
        var valid = true;
        var release_id = $('#release_id').val();
        var id_db = $('#id_db').val();
        valid = valid && checkLength( table_db, "Table Name", 1, 50 );
        valid = valid && checknull( query, "Query");
        if ( valid )
        {
            $.ajax({
              beforeSend: function(){//Sự kiện được kích hoạt trươc khi một Ajax request được bắt đầu
              // trước khi truyền data được bắn lên là data{} với post cho bên controller và được trả về 1 chuỗi json
              },  
              delay:0,
              url: '/releases/addReleaseDB',
              type: 'POST',
              dataType: 'json',
              data: {"id":id_db,"release_id" : release_id,"editor":$('#editor_db').val(),"table_db": table_db.val(),"status": $('#status_db').val(),"query_db":$("#query").val()},
              success: function(data){ // khi thành công nhân được data msg
                       if(data.status == true){
                        if(id_db!=0)
                        {
                            alert("Your Option Has Been Edit");
                        }
                        else{
                            alert(data.msg);
                        }
                        $("#example3").load(location.href + " #example3");
                        dialog_db.dialog( "close" );
                        }
                        else{
                              $('#error_addReleaseDB').val((updateTips(data.text)));
                              $('#error_addReleaseDB').addClass( "ui-state-error" );
                              updateTips(data.msg);
                    }
                }
            })
        }
    }
    dialog_db = $( "#dialog-form-db" ).dialog({
        autoOpen: false,
        height: 600,
        width: 800,
        modal: true,
        buttons: {
            "Save DB": addReleaseDB,
            Cancel: function() {
              dialog_db.dialog( "close" );
            }
        },
            close: function() {
              form_db[0].reset();
            },
     hide: {
                effect: "explode",
                duration: 1000
      }  
    });
    form_db = dialog_db.find( "form" ).on( "submit", function( event ) {
        event.preventDefault();
        addReleaseDB();
    });   
    $( "#add_databases" ).button().on( "click", function() {
        $(".ui-dialog-title").html("Add DataBase Release");
        $('#id_db').val(0);
        dialog_db.dialog( "open" );
        dialog_db.effect( "slide", "slow" );
        $('#error_addReleaseDB').html('');
        allFields.removeClass( "error_addReleaseDB" );
        allFields.removeClass( "ui-state-error" );
    });
});

function EditReleaseDB(id)
{
    $(".ui-dialog-title").html("Edit DataBase Release");
    $("#id_db").val(id);
    var table = $("#table_db" + id).html();
    $("#table_db").val(table);
    var query = $("#dialog-show-query" + id).html();
    $("#query").val(query);
     var status = $("#status_db" + id).val();
    $("#status_db").val(status);
    var editor = $("#editor_db" + id).val();
    $("#editor_db").val(editor);
    $( "#dialog-form-db" ).dialog("open");
    $( "#dialog-form-db" ).effect( "slide", "slow" );
    $('#error_addReleaseDB').html('');
    allFields.removeClass( "error_addReleaseDB" );
    allFields.removeClass( "ui-state-error" );
}

function show_query(id) {
    $( "#dialog-show-query" +id ).dialog({
        autoOpen: true,
        height:250,
        width:450,
        modal: true,
        show: {
            effect: "slide",
            duration: 500
        },
        hide: {
            effect: "explode",
            duration: 500
      }
    });
    $( "#dialog-show-query"+id ).effect( "slide", "slow" );;
} 

function DeleteReleaseDB(id) {
    $( "#dialog-confirm-db" ).dialog({
        height:250,
        width:350,
        modal: true,
        buttons: {
            "Delete": function() {
                $.ajax({
                    beforeSend: function(){
                    },
                    delay: 0,
                    url: '/releases/DeleteReleaseDB',
                    type: 'POST',
                    dataType: 'json',
                    data : { 'id' : id },
                    success: function(data){
                        if(data.status == false){
                            alert(data.msg);
                        }
                        $("#example3").load(location.href + " #example3");
                        $( "#dialog-confirm-db" ).dialog( "close" );
                    }
                });
            },
            Cancel: function() {
                $( this ).dialog( "close" );
            }
        },
        hide: {
            effect: "explode",
            duration: 500
      }
    });
}
