$(document).ready(function(){
    $("#memberModal").on('show.bs.modal', function(event){
        var button = $(event.relatedTarget);// trả về kết quả khi click chuột
        var title = button.data('title'); // tạo biến gán bằng dữ liệu của button có tên dữ liệu là title
        if (title === 'edit') {
            var taskId =  button.data('member');
            $("#taskId").val(taskId);
            var redmine_id = $("#redmine_id" + taskId).html();
            $("#redmine_id").val(redmine_id);
            var assigned = $("#assigned" + taskId).val();
            $("#assigned").val(assigned);
            var member_review = $("#member_review" + taskId).val();
            $("#member_review").val(member_review);
            var project_id = $("#project_id" + taskId).val();
            $("#project_id").val(project_id);
            var task_status = $("#task_status" + taskId).val();
            $("#task_status").val(task_status);
            var title = $("#title_task" + taskId).html();
            $("#title_task").val(title);
            var task_goal = $("#task_goal" + taskId).html();
            $("#task_goal").val(task_goal);
            //var doc_file = $("#doc_file" + taskId).val();
            //alert(doc_file);
            //document.getElementById("doc_file").innerHTML = doc_file;
            //$("#doc_file").val(doc_file);
            var test_cases = $("#test_cases" + taskId).html();
            $("#test_cases").val(test_cases);
            var review_testcase = $("#review_testcase" + taskId).html();
            $("#review_testcase").val(review_testcase);
            var merge_req_test = $("#merge_req_test" + taskId).html();
            $("#merge_req_test").val(merge_req_test);
            var merge_reg_staging = $("#merge_reg_staging" + taskId).html();
            $("#merge_reg_staging").val(merge_reg_staging);
            var merge_reg_live = $("#merge_reg_live" + taskId).html();
            $("#merge_reg_live").val(merge_reg_live);
            $('div.invalid-msg').html('');
        } else {
            $("#taskId").val(0);
        }
    });

     


    // Đưa form về mặc định khi add new task
    $("#memberModal").on('show.bs.modal', function(event){
        var button = $(event.relatedTarget);
        var title = button.data('title'); 
        if (title === 'add') {
            $("#redmine_id").val("");
            $("#assigned").val(0);
            $("#member_review").val(0);
            $("#project_id").val(0);
            $("#task_status").val(0);
            $("#title_task").val("");
            $("#task_goal").val("");
            $("#doc_file").val("");
            $("#test_cases").val("");
            $("#review_testcase").val("");
            $("#merge_req_test").val("");
            $("#merge_reg_staging").val("");
            $("#merge_reg_live").val("");
            $('div.invalid-msg').html('');
            $('div.msg').html(''); 
        }
    });




    // edit Function_changes
    $("#func_chitiet").on('show.bs.modal', function(event){
        var button = $(event.relatedTarget);
        var title = button.data('title'); 
        if (title === 'edit_func') {
            var id_func =  button.data('member');
            $("#id_func").val(id_func);
            var func = $("#func" + id_func).html();
            $('#func').val(func);
            var description = $("#description_func" + id_func).html();
            $('#description_func').val(description);
            var note = $("#note_func" + id_func).html();
            $('#note_func').val(note);
            var change_type = $("#change_type_func" + id_func).val();
            $('#change_type_func').val(change_type);
            $('div.invalid-msg').html('');
            }else{
                $("#id_func").val(0);
            }
    });


    // edit Database
    $("#data_chitiet").on('show.bs.modal', function(event){
        var button = $(event.relatedTarget);
        var title = button.data('title'); 
        if (title === 'edit_data') {
            var id_data =  button.data('member');
            $("#id_data").val(id_data);
            var table_name = $("#table_name" + id_data).html();
            $('#table_name').val(table_name);
            var change_type = $("#change_type_data" + id_data).val();
            $('#change_type_data').val(change_type);
            var description = $("#description_data" + id_data).html();
            $('#description_data').val(description);
            var queries = $("#queries" + id_data).html();
            $('#queries').val(queries);
            var optimized = $("#optimized" + id_data).val();
            $('#optimized').val(optimized);
            var note = $("#note_data" + id_data).html();
            $('#note_data').val(note);
            $('div.invalid-msg').html('');
            }else{
                $("#id_data").val(0);
            }
    });


    // edit Process step
    $("#step_chitiet").on('show.bs.modal', function(event){
        var button = $(event.relatedTarget);
        var title = button.data('title'); 
        if (title === 'edit_step') {
            var id_step =  button.data('member');
            $("#id_step").val(id_step);
            var title = $("#title_step" + id_step).html();
            $('#title_step').val(title);
            var description = $("#description_step" + id_step).html();
            $('#description_step').val(description);
            var change_files = $("#change_files" + id_step).html();
            $('#change_files').val(change_files);
            var editor = $("#editor" + id_step).val();
            $('#editor').val(editor);
            var status = $("#status" + id_step).val();
            $('#status').val(status);
            $('div.invalid-msg').html('');
            }else{
                $("#id_step").val(0);
            }
    });

    // Đưa form về mặc định khi add new function changes
    $("#func_chitiet").on('show.bs.modal', function(event){
        var button = $(event.relatedTarget);
        var title = button.data('title'); 
        if (title === 'add_func') {
            $("#func").val("");
            $("#change_type_func").val(0);
            $("#description_func").val("");
            $("#note_func").val("");
            var task_id_func =  button.data('member');
            $("#task_id_func").val(task_id_func);
            $('div.invalid-msg').html('');
            $('div.msg').html(''); 
        }
    });


    // Đưa form về mặc định khi add new Process step
    $("#step_chitiet").on('show.bs.modal', function(event){
        var button = $(event.relatedTarget);
        var title = button.data('title'); 
        if (title === 'add_step') {
            $("#title_step").val("");
            $("#editor").val(0);
            $("#status").val(0);
            $("#description_step").val("");
            $("#change_files").val("");
            var task_id_step =  button.data('member');
            $("#task_id_step").val(task_id_step);
            $('div.invalid-msg').html('');
            $('div.msg').html(''); 
        }
    });


    // Đưa form về mặc định khi add new Database
    $("#data_chitiet").on('show.bs.modal', function(event){
        var button = $(event.relatedTarget);
        var title = button.data('title'); 
        if (title === 'add_data') {
            $('#table_name').val("");
            $('#change_type_data').val(0);
            $('#optimized').val(0);
            $('#description_data').val("");
            $('#queries').val("");
            $('#note_data').val("");
            var task_id_data =  button.data('member');
            $("#task_id_data").val(task_id_data);
            $('div.invalid-msg').html('');
            $('div.msg').html(''); 
        }
    });




    // add function_changes
    $("#add_function_changes").click(function() {
        var id_func = $.trim($("#id_func").val());
        var task_id_func = $("#task_id_func").val();
        var func = $.trim($("#func").val());
        var description = $.trim($("#description_func").val());
        var change_type = $.trim($("#change_type_func").val());
        var note = $.trim($("#note_func").val());

        // Validate bằng jquery trên Client
        var checkPass = 1;
         $('.inputText2').each(function (){
                if($.trim($(this).val()) == ''){
                    
                    checkPass = 0;
                    $(this).parent().parent().next('div.invalid-msg').html('<em>Vui lòng không được để trống</em>').css('color','red'); 
                    
                }else{
                        $(this).parent().parent().next('div.invalid-msg').html('');
                    }       
                                 
        });    
        $('.alo2').each(function (){
                if($(this).val() == 0) {
                    
                    checkPass = 0;
                    $(this).parent().parent().next('div.invalid-msg').html('<em>Vui lòng chọn</em>').css('color','red'); 
                }else{
                        $(this).parent().parent().next('div.invalid-msg').html('');
                       
                    }       
                                 
        });    
         // Post form bằng ajax
        if (checkPass)
        {
            $.ajax({
                beforeSend: function(){
                },
                delay: 0,
                url: '/tasks/saveFunc',
                type: 'POST',
                dataType: 'json',
                data : { id : id_func, task_id : task_id_func, func : func, change_type : change_type, description : description, note : note },
                success: function(data){
                    if(data.status == true){
                        $("#listfunc").load(location.href + " #listfunc");
                        document.getElementById("myButton_func").click();
                        $('div.msg_func').html('(' + data.msg + ')').css('color','green').fadeIn('fast').fadeOut(5000);
                    }else{
                        $('div.msg').html(data.msg).css('color','red');           
                    }
                }
            });
        }

    });


    // add Process step
    $("#add_step").click(function() {
        var id_step = $.trim($("#id_step").val());
        var task_id_step = $("#task_id_step").val();
        var description = $.trim($("#description_step").val());
        var title = $.trim($("#title_step").val());
        var editor = $.trim($("#editor").val());
        var status = $.trim($("#status").val());
        var change_files = $.trim($("#change_files").val());

        // Validate bằng jquery trên Client
        var checkPass = 1;
         $('.inputText3').each(function (){
                if($.trim($(this).val()) == ''){
                    
                    checkPass = 0;
                    $(this).parent().parent().next('div.invalid-msg').html('<em>Vui lòng không được để trống</em>').css('color','red'); 
                    
                }else{
                        $(this).parent().parent().next('div.invalid-msg').html('');
                    }       
                                 
        });    
        $('.alo3').each(function (){
                if($(this).val() == 0) {
                    
                    checkPass = 0;
                    $(this).parent().parent().next('div.invalid-msg').html('<em>Vui lòng chọn</em>').css('color','red'); 
                }else{
                        $(this).parent().parent().next('div.invalid-msg').html('');
                       
                    }       
                                 
        });    
         // Post form bằng ajax
        if (checkPass)
        {
            $.ajax({
                beforeSend: function(){
                },
                delay: 0,
                url: '/tasks/saveStep',
                type: 'POST',
                dataType: 'json',
                data : { id : id_step, task_id : task_id_step, title : title, change_files : change_files, description : description, editor : editor, status : status },
                success: function(data){
                    if(data.status == true){
                        $("#liststep").load(location.href + " #liststep");
                        document.getElementById("myButton_step").click();
                        $('div.msg_step').html('(' + data.msg + ')').css('color','green').fadeIn('fast').fadeOut(5000);
                    }else{
                        $('div.msg').html(data.msg).css('color','red');              
                    }
                }
            });
        }

    });




    // add Database
    $("#add_data").click(function() {
        var id_data = $.trim($("#id_data").val());
        var task_id_data = $("#task_id_data").val();
        var change_type = $.trim($("#change_type_data").val());
        var table_name = $.trim($("#table_name").val());
        var description = $.trim($("#description_data").val());
        var queries = $.trim($("#queries").val());
        var optimized = $.trim($("#optimized").val());
        var note = $.trim($("#note_data").val());

        // Validate bằng jquery trên Client
        var checkPass = 1;
         $('.inputText4').each(function (){
                if($.trim($(this).val()) == ''){
                    
                    checkPass = 0;
                    $(this).parent().parent().next('div.invalid-msg').html('<em>Vui lòng không được để trống</em>').css('color','red'); 
                    
                }else{
                        $(this).parent().parent().next('div.invalid-msg').html('');
                    }       
                                 
        });    
        $('.alo4').each(function (){
                if($(this).val() == 0) {
                    
                    checkPass = 0;
                    $(this).parent().parent().next('div.invalid-msg').html('<em>Vui lòng chọn</em>').css('color','red'); 
                }else{
                        $(this).parent().parent().next('div.invalid-msg').html('');
                       
                    }       
                                 
        });    
         // Post form bằng ajax
        if (checkPass)
        {
            $.ajax({
                beforeSend: function(){
                },
                delay: 0,
                url: '/tasks/saveDatabase',
                type: 'POST',
                dataType: 'json',
                data : { id : id_data, task_id : task_id_data, table_name : table_name, change_type : change_type, description : description, queries : queries, optimized : optimized, note : note },
                success: function(data){
                    if(data.status == true){
                        $("#listdata").load(location.href + " #listdata");
                        document.getElementById("myButton_data").click();
                        $('div.msg_data').html('(' + data.msg + ')').css('color','green').fadeIn('fast').fadeOut(5000);
                    }else{
                        $('div.msg').html(data.msg).css('color','red');               
                    }
                }
            });
        }

    });


   
    // Add New task
    $("#addTask").click(function() {
        
        var redmine_id = $.trim($("#redmine_id").val());
        var title = $.trim($("#title_task").val());
        var task_goal = $.trim($("#task_goal").val());
        var assigned = $.trim($("#assigned").val());
        var doc_file = $.trim($("#doc_file").val());
        var test_cases = $.trim($("#test_cases").val());
        var review_testcase = $.trim($("#review_testcase").val());
        var member_review = $.trim($("#member_review").val());
        var project_id = $.trim($("#project_id").val());
        var task_status = $.trim($("#task_status").val());
        var merge_req_test = $.trim($("#merge_req_test").val());
        var merge_reg_staging = $.trim($("#merge_reg_staging").val());
        var merge_reg_live = $.trim($("#merge_reg_live").val());
        var taskId = $.trim($("#taskId").val());
        
        // Validate bằng jquery trên Client
        var checkPass = 1;
        
         $('.inputText1').each(function (){
                if($.trim($(this).val()) == '') {
                    
                    checkPass = 0;
                    $(this).parent().parent().next('div.invalid-msg').html('<em>Vui lòng không được để trống</em>').css('color','red'); 
                }else{
                        $(this).parent().parent().next('div.invalid-msg').html('');
                       
                    }       
                                 
        });    
        $('.alo').each(function (){
                if($(this).val() == 0) {
                    
                    checkPass = 0;
                    $(this).parent().parent().next('div.invalid-msg').html('<em>Vui lòng chọn</em>').css('color','red'); 
                }else{
                        $(this).parent().parent().next('div.invalid-msg').html('');
                       
                    }       
                                 
        });    
         // Post form bằng ajax
        if (checkPass)
        {
            $.ajax({


                beforeSend: function(){
                },
                delay: 0,
                url: '/tasks/save',
                type: 'POST',
                dataType: 'json',
                data : { redmine_id : redmine_id, assigned : assigned,  title: title, task_goal : task_goal, doc_file : doc_file, test_cases : test_cases, review_testcase : review_testcase, member_review : member_review, project_id : project_id, merge_req_test : merge_req_test, merge_reg_staging : merge_reg_staging, merge_reg_live : merge_reg_live, status : task_status, id : taskId },
                success: function(data){
                    if(data.status == true){
                        window.location.reload(true);
                        document.getElementById("myButton").click();
                        $('div.msg1').html('(' + data.msg + ')').css('color','green').fadeIn('fast').fadeOut(5000);
                    }else{
                        $('div.msg').html(data.msg).css('color','red');       
                    }
                }
            });
        }

    });

});


function deleteTask(taskId) {
    $( "#dialog-confirm" ).dialog({
        resizable: false,
        height:250,
        width:350,
        modal: true,
        buttons: {
            "Delete": function() {
                $.ajax({
                    beforeSend: function(){
                    },
                    delay: 0,
                    url: '/tasks/delete',
                    type: 'POST',
                    dataType: 'json',
                    data : { id : taskId},
                    success: function(data){
                        if(data.status == true){
                            $("#list").load(location.href + " #list");
                            $('div.msg1').html('(' + data.msg + ')').css('color','green').fadeIn('fast').fadeOut(5000);
                        }else{
                            alert(data.msg);
                        }
                        $("#dialog-confirm").dialog( "close" );
                    }
                });
            },
            Cancel: function() {
                $( this ).dialog( "close" );
            }
        }
    });
}


function deleteFunc(taskFunc) {
    $( "#dialog-confirm-func" ).dialog({
        resizable: false,
        height:250,
        width:350,
        modal: true,
        buttons: {
            "Delete": function() {
                $.ajax({
                    beforeSend: function(){
                    },
                    delay: 0,
                    url: '/tasks/deleteFunc',
                    type: 'POST',
                    dataType: 'json',
                    data : { id : taskFunc},
                    success: function(data){
                        if(data.status == true){
                            $("#listfunc").load(location.href + " #listfunc");
                            $('div.msg_func').html('(' + data.msg + ')').css('color','green').fadeIn('fast').fadeOut(5000);
                        }else{
                            alert(data.msg);
                        }
                        $("#dialog-confirm-func").dialog( "close" );
                    }
                });
            },
            Cancel: function() {
                $( this ).dialog( "close" );
            }
        }
    });
}



function deleteStep(taskStep) {
    $( "#dialog-confirm-step" ).dialog({
        resizable: false,
        height:250,
        width:350,
        modal: true,
        buttons: {
            "Delete": function() {
                $.ajax({
                    beforeSend: function(){
                    },
                    delay: 0,
                    url: '/tasks/deleteStep',
                    type: 'POST',
                    dataType: 'json',
                    data : { id : taskStep},
                    success: function(data){
                        if(data.status == true){
                            $("#liststep").load(location.href + " #liststep");
                            $('div.msg_step').html('(' + data.msg + ')').css('color','green').fadeIn('fast').fadeOut(5000);
                        }else{
                            alert(data.msg);
                        }
                        $("#dialog-confirm-step").dialog( "close" );
                    }
                });
            },
            Cancel: function() {
                $( this ).dialog( "close" );
            }
        }
    });
}


function deleteDatabase(taskData) {
    $( "#dialog-confirm-data" ).dialog({
        resizable: false,
        height:250,
        width:350,
        modal: true,
        buttons: {
            "Delete": function() {
                $.ajax({
                    beforeSend: function(){
                    },
                    delay: 0,
                    url: '/tasks/deleteDatabase',
                    type: 'POST',
                    dataType: 'json',
                    data : { id : taskData},
                    success: function(data){
                        if(data.status == true){
                            $("#listdata").load(location.href + " #listdata");
                            $('div.msg_data').html('(' + data.msg + ')').css('color','green').fadeIn('fast').fadeOut(5000);
                        }else{
                            alert(data.msg);
                        }
                        $("#dialog-confirm-data").dialog( "close" );
                    }
                });
            },
            Cancel: function() {
                $( this ).dialog( "close" );
            }
        }
    });
}