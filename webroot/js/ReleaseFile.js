$(document).ready(function(){
  var dialog_file,
  File_Path = $('#File_Path');
  coder = $('#coder'),
  allFields = $( [] ).add( File_Path ).add(coder),
  tips = $( "#error_add_file" );
    function updateTips( t ){// animation của validate form bằng jquery với tham số truyền vào là 1 chuỗi
        tips
            .text( t )
            .addClass( "ui-state-highlight" );
            setTimeout(function() {
            tips.removeClass( "ui-state-highlight", 5000 );
            });
    }
    function checkLength( o, n, min, max ) {//check lenght xâu param là đối tượng có id o,chuỗi n báo lỗi,min và max lenght
        if ( o.val().length > max || o.val().length < min ) {
            o.addClass( "ui-state-error" );
            updateTips( "Length of " + n + " must be between " + min + " and " + max + "." );

            return false;
        }else{
            return true;
        } 
    }
    function checkRegexp( o, regexp, n ) {
      if ( !( regexp.test( o.val() ) ) ) {
        o.addClass( "ui-state-error" );
        updateTips( n );
        return false;
      } else {
        return true;
      }
    }
    function checknull( o, n) { //check null
        if ( !o.val()) {
            o.addClass( "ui-state-error" );
            updateTips( "You must add data in " +n );
            return false;
        }else{
            return true;
        }
    }
    function addReleaseFile(){
        var valid =true;
        var id_file = $("#id_file").val();
        var release_id = $('#release_id').val();
        var state = $('#state').val();
        valid = valid && checkLength( File_Path, "File_Path", 1,255 );
        valid = valid && checkRegexp(File_Path, /^[a-zA-Z0-9-.:_\/\\]+$/i, "This File_Path Not Valid");
        valid = valid && checknull(coder,"Coder");
        if(valid ){
            $.ajax({
                beforeSend : function(){
                },//Sự kiện được kích hoạt trươc khi một Ajax request được bắt đầu
                delay:0,
                url: '/releases/addReleaseFile',
                type: 'POST',
                dataType: 'json',
                data: {"id" : id_file,"release_id" : release_id , "file_path" : File_Path.val(), "state" : state, "coder" : $('#coder').val()},
                success: function(data){
                    if(data.status == true){
                        if(id_file!=0)
                        {
                            alert("Your File Has Been Edit");
                        }
                        else{
                            alert(data.msg);
                        }
                        $("#example2").load(location.href + " #example2");
                        dialog_file.dialog( "close" );
                    }
                    else{
                      $('#error_add_file').val((updateTips(data.text)));
                      $('#error_add_file').addClass( "ui-state-error" );
                      updateTips(data.msg);
                    }
                }
            });
        }
      }
    dialog_file = $( "#dialog-form-file" ).dialog({
        autoOpen: false,
        height: 450,
        width: 500,
        modal: true,
        buttons: {
            "Save File": addReleaseFile,
            Cancel: function() {
              dialog_file.dialog( "close" );
            }
        },
            close: function() {
              form_file[0].reset();
            },
             hide: {
                effect: "explode",
                duration: 1000
      }  
    });
    form_file = dialog_file.find( "form" ).on( "submit", function( event ) {
        event.preventDefault();
        addReleaseFile();
    });   
    $( "#addFile" ).button().on( "click", function() {
        $(".ui-dialog-title").html("Add File");
        $("#id_file").val(0);
        dialog_file.dialog( "open" );
        dialog_file.effect( "slide", "slow" );
        $('#error_add_file').html('');
        allFields.removeClass( "error_add_file" );
        allFields.removeClass( "ui-state-error");
    });
});

function EditReleaseFile(id)
{
    $("#id_file").val(id);
    var File_Path = $("#File_Path" + id).html();
    $("#File_Path").val(File_Path);
    var state = $("#state" + id).val();
    $("#state").val(state);
    var coder = $("#coder" + id).val();
    $("#coder").val(coder);
    $(".ui-dialog-title").html("Edit File");
    $( "#dialog-form-file" ).dialog("open");
    $( "#dialog-form-file" ).effect( "slide", "slow" );
    $('#error_add_file').html('');
    allFields.removeClass( "error_add_file" );
    allFields.removeClass( "ui-state-error");
}

function DeleteReleaseFile(id) {
    $( "#dialog-confirm-file" ).dialog({
        height:250,
        width:350,
        modal: true,
        buttons: {
            "Delete": function() {
                $.ajax({
                    beforeSend: function(){
                    },
                    delay: 0,
                    url: '/releases/DeleteReleaseFile',
                    type: 'POST',
                    dataType: 'json',
                    data : { 'id' : id },
                    success: function(data){
                        if(data.status == false){
                            alert(data.msg)
                        }
                        $("#example2").load(location.href + " #example2");
                        $( "#dialog-confirm-file" ).dialog( "close" );
                    }
                });
            },
            Cancel: function() {
                $( this ).dialog( "close" );
            }
        },
        hide: {
            effect: "explode",
            duration: 500
      }
    });
} 
